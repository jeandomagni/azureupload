﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft;
namespace AzureUpload.Core.Models
{
    [Serializable, XmlType(TypeName = "fileWatcher")]
    public class FileWatcherConfig
    {
        [XmlAttribute("outputRootPath"), JsonProperty(PropertyName = "outputRootPath")]
        public string OutputRootPath { get; set; }

        [XmlElement("client"), JsonProperty(PropertyName = "clients")]
        public List<ClientConfig> Clients { get; set; }

        [XmlAttribute("status"), JsonProperty(PropertyName = "status"), JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }
    }

    public enum Status
    {
        Running,
        Stopped,
        Paused
    }
}
