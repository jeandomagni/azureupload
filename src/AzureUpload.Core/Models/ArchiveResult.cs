﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureUpload.Core.Models
{
    public class ArchiveResult
    {
        public bool Success { get; set; }
        public string Path { get; set; }
        public Exception Exception { get; set; }
        public UploadInfo UploadInfo { get; set; }
    }
}
