﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Core.Models
{
    
    public class ClientThreadEvent
    {
        [JsonProperty(PropertyName = "threadId")]
        public int ThreadId { get; set; }
        [JsonProperty(PropertyName = "retries")]
        public int Retries { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type {get; set; }
        [JsonProperty(PropertyName = "fileName")]
        public string FileName {get; set; }
        [JsonProperty(PropertyName = "client")]
        public string Client {get; set; }
        [JsonProperty(PropertyName = "fileType")]
        public string FileType { get; set; }
    }
}
