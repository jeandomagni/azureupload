﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Core.Models
{
    public class UploadInfo
    {
        public string BlobContainer { get; set; }
        public string WorkerType { get; set; }
        public FileType FileType { get; set; }
        public string Path { get; set; }
        public string ClientName { get; set; }
        public string FileTypeOverride { get; set; }
        public string OutputSubFolder { get; set; }
        public string ArchiveRootOverride { get; set; }
        public int Retries { get; set; }
        public string QueueName { get; set; }
        public long FileSize { get; set; }
        public bool Archive { get; set; }
    }
}
