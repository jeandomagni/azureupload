﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AzureUpload.Core.Models
{
    [Serializable, XmlType(TypeName = "file")]
    public class FileConfig
    {
        /// <summary>
        /// Default values supplied during deserialization since DefaultValueAttribute only applies to serialization.
        /// </summary>
        public FileConfig()
        {
            Archive = true;
            Upload = true;
            QueueContainer = "worker05";
            BlobContainer = "workerincoming";
        }

        [XmlAttribute("fileType"), JsonProperty(PropertyName = "fileType"), JsonConverter(typeof(StringEnumConverter))]
        public FileType FileType { get; set; }

        [XmlAttribute("outputSubFolder"), JsonProperty(PropertyName = "outputSubFolder")]
        public string OutputSubFolder { get; set; }

        [XmlAttribute("archiveRootOverride"), JsonProperty(PropertyName = "archiveRootOverride")]
        public string ArchiveRootOverride { get; set; }

        [XmlAttribute("dropFolderPathOverride"), JsonProperty(PropertyName = "dropFolderPathOverride")]
        public string DropFolderPathOverride { get; set; }

        [XmlAttribute("fileTypeOverride"), JsonProperty(PropertyName = "fileTypeOverride")]
        public string FileTypeOverride { get; set; }

        [XmlAttribute("blobContainer"), DefaultValue("workerincoming"), JsonProperty(PropertyName = "blobContainer")]
        public string BlobContainer { get; set; }

        [XmlAttribute("queueContainer"), DefaultValue("worker05"), JsonProperty(PropertyName = "queueContainer")]
        public string QueueContainer { get; set; }

        [XmlAttribute("workerType"), JsonProperty(PropertyName = "workerType")]
        public string WorkerType { get; set; }

        [XmlAttribute("filterPattern"), JsonProperty(PropertyName = "filterPattern")]
        public string FilterPattern { get; set; }
        
        [XmlAttribute("archive"), DefaultValue(true), JsonProperty(PropertyName = "archive")]
        public bool Archive { get; set; }
        
        [XmlAttribute("upload"), DefaultValue(true), JsonProperty(PropertyName = "upload")]
        public bool Upload { get; set; }
    }

    public enum FileType
    {
        None,
        Invoice,
        Adjustment,
        CreditReturn,
        Dispense,
        Qoh,
        Delete
    }
}
