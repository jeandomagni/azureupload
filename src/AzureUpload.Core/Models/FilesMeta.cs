﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureUpload.Core.Models
{
    [ProtoContract]
    public class FileMeta
    {
        [ProtoMember(1, IsPacked = true)]
        public List<int> SiteNumbers { get; set; }
        [ProtoMember(2, IsPacked = true)]
        public List<int> Sizes { get; set; }
    }
}
