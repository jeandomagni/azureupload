﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureUpload.Core.Models
{
    public enum ArchiveRetryReason
    {
        Unknown,
        Timeout,
        FileInUse
    }
    public class ArchiveInfo
    {
        public ArchiveRetryReason RetryReason { get; set; }
        public string Path { get; set; }
        public int Retries { get; set; }
    }
}
