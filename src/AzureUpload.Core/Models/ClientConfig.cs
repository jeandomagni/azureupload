﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AzureUpload.Core.Models
{
    [Serializable, XmlType(TypeName= "client")]
    public class ClientConfig
    {
        private static HashSet<char> charValidator = new HashSet<char>(Path.GetInvalidFileNameChars().Union(new[] { ' ', '\'' }));

        private string name;

        [XmlAttribute("name"), JsonProperty(PropertyName = "name")]
        public string Name
        {
            get { return name; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    name = string.Concat(value.ToCharArray().Where(x => !charValidator.Contains(x)));
                }
            }
        }

        [XmlElement("file"), JsonProperty(PropertyName = "files")]
        public List<FileConfig> Files { get; set; }

        [XmlAttribute("dropFolderPath"), JsonProperty(PropertyName = "dropFolderPath")]
        public string DropFolderPath { get; set; }
        
        [XmlAttribute("chainId"), JsonProperty(PropertyName = "chainId")]
        public int ChainId { get; set; }

        [XmlAttribute("status"), JsonProperty(PropertyName = "status"), JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }
    }
}
