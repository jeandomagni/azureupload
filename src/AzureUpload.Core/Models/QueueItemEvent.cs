﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AzureUpload.Core.Models
{
    public enum QueueType
    {
        Unknown,
        DownloadQueue,
        UploadQueue,
        ArchiveQueue
    }

    public enum EventType
    {
        Added,
        BeganWork,
        Removed,
        Error
    }

    public class QueueItemEvent
    {
        const int MEGABYTE_IN_BYTES = 1024 * 1024;
        const int KILOBYTE_IN_BYTES = 1024;

        public QueueItemEvent(UploadInfo info, QueueType type, EventType e)
        {
            Archive = info.Archive;
            CreatedUtc = DateTime.UtcNow;
            FileName = Path.GetFileName(info.Path);
            FileType = info.FileTypeOverride ?? info.FileType.ToString();
            ClientName = info.ClientName;
            QueueType = type;
            EventType = e;
            Retries = info.Retries;
           
            if (info.FileSize >= MEGABYTE_IN_BYTES)
                FileSize = string.Format("{0:0} mb", info.FileSize / MEGABYTE_IN_BYTES);
            else if (info.FileSize >= KILOBYTE_IN_BYTES)
                FileSize = string.Format("{0:0} kb", info.FileSize / KILOBYTE_IN_BYTES);
            else
                FileSize = string.Format("{0:0} b", info.FileSize);
            
        }
        public string Id 
        {
            get
            {
                // TODO: better way? the files get renamed mid process with a gz thus making name not a good identifier
                return string.Format("{0}-{1}-{2}", ClientName, FileType, (FileName ?? "").Replace(".gz", ""));
            }
        }
        public bool Archive { get; set; }
        public string ClientName { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public DateTime CreatedUtc { get; set; }
        public int Count { get; set; }
        public int Retries { get; set; }
        [JsonProperty(PropertyName = "QueueType"), JsonConverter(typeof(StringEnumConverter))]
        public QueueType QueueType { get; set; }
        [JsonProperty(PropertyName = "EventType"), JsonConverter(typeof(StringEnumConverter))]
        public EventType EventType { get; set; }


        public string Exception { get; set; }
    }
}
