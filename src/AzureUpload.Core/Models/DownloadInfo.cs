﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureUpload.Core.Models
{
    public class DownloadInfo
    {
        public DateTime? LastReadAttemptUtc { get; set; }
        public int Retries { get; set; }
    }
}
