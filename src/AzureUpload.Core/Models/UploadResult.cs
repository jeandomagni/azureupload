﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Core.Models
{
    public class CompressionInfo
    {
        public string FileName { get; set; }
        public string TempPath { get; set; }
    }

    public class UploadResult
    {
        public bool WasCompressed { get { return CompressionInfo != null; } }
        public CompressionInfo CompressionInfo { get; set; }
        public bool Success { get; set; }
        public UploadInfo UploadInfo { get; set; }
        public Exception Exception { get; set; }
        public string BlobUri { get; set; }
        public string WorkerMessage { get; set; }
    }
}
