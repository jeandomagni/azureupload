﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AzureUpload.Core.Helpers
{
    public static class S3KeyExtensions
    {
        public static bool IsPackaged(this string p)
        {
            return p.EndsWith(".tar.gz") || p.EndsWith(".zip");
        }

        static Regex siteRegex = new Regex(@"_(\d{1,7})_");
        public static int GetSiteNumber(this string p)
        {
            var fileName = p.Substring(p.LastIndexOf('/') + 1);
            int siteNum = 0;

            var match = siteRegex.Match(fileName);

            if (match.Groups[1].Success)
                int.TryParse(match.Groups[1].Value, out siteNum);

            return siteNum;
        }
    }
}
