﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AzureUpload.Core.Helpers
{
    public static class Util
    {
        public static T LoadFile<T>(string path)
        {
            var ser = new XmlSerializer(typeof(T));
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(fs, Encoding.Default))
            {
                return (T)ser.Deserialize(sr);
            } 
        }

        public static void SaveFile<T>(string path, T data)
        {
            var text = string.Empty;
            using (var writer = new StringWriter())
            {
                var xns = new XmlSerializerNamespaces();
                xns.Add(string.Empty, string.Empty);
                var ser = new XmlSerializer(typeof(T));
                ser.Serialize(writer, data, xns);
                text = writer.ToString();
            }

            if(!string.IsNullOrWhiteSpace(text))
                File.WriteAllText(path, text, Encoding.Default);
        }

        public static string Setting(string name)
        {
            return Setting<string>(name);
        }

        public static T Setting<T>(string name)
        {
            var appSettings = ConfigurationManager.AppSettings;
            if(appSettings == null)
                return default(T);

            var strValue = "";

            if (string.IsNullOrEmpty(strValue = appSettings[name]))
                return default(T);

            var value = TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(strValue);

            return (T)value;

        }

        public static long GetFileSize(string path)
        {
            FileInfo fi = new FileInfo(path);

            return fi.Exists ? fi.Length : 0L;
        }

        const int MEGABYTE_IN_BYTES = 1024 * 1024;
        const int KILOBYTE_IN_BYTES = 1024;
        
        public static string GetFileSize(long size)
        {
            if (size >= MEGABYTE_IN_BYTES)
                return string.Format("{0:0} mb", size / MEGABYTE_IN_BYTES);
            else if (size >= KILOBYTE_IN_BYTES)
                return string.Format("{0:0} kb", size / KILOBYTE_IN_BYTES);
            else
                return string.Format("{0:0} b", size);
        }

    }
}
