﻿using AzureUpload.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AzureUpload.Core.Helpers
{
    public static class PathHelper
    {
        public static string GetArchiveDir(string outputRoot, ClientConfig clientNode, FileConfig fileNode)
        {
            var archiveRoot = fileNode.ArchiveRootOverride ??
                Path.Combine(outputRoot, clientNode.Name, "Archive");

            var archivePath = Path.Combine(archiveRoot,
                fileNode.FileTypeOverride ?? fileNode.FileType.ToString(),
                fileNode.OutputSubFolder ?? string.Empty);

            return archivePath;
        }

        public static string GetArchiveDir(string outputRoot, UploadInfo info)
        {
             var archiveRoot = info.ArchiveRootOverride ??
                Path.Combine(outputRoot, info.ClientName, "Archive");

             var archivePath = Path.Combine(archiveRoot,
                 info.FileTypeOverride ?? info.FileType.ToString(),
                 info.OutputSubFolder ?? string.Empty);
             
            return archivePath;
        }

        public static string GetExceptionDir(string outputRoot, ClientConfig clientNode, FileConfig fileNode)
        {
            var exceptionPath = Path.Combine(outputRoot,
                clientNode.Name,
                "Exception",
                fileNode.FileTypeOverride ?? fileNode.FileType.ToString(),
                fileNode.OutputSubFolder ?? string.Empty
            );

            return exceptionPath;
        }

        public static string GetExceptionDir(string outputRoot, UploadInfo info)
        {
            var exceptionPath = Path.Combine(outputRoot,
                info.ClientName,
                "Exception",
                info.FileTypeOverride ?? info.FileType.ToString(),
                info.OutputSubFolder ?? string.Empty
            );

            return exceptionPath;
        }
    }
}
