﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Core.Helpers
{
    public static class MimeTypes
    {
        private static Dictionary<string, string> _mimeTypes = new Dictionary<string, string>()
        {
            { "asf", "video/x-ms-asf" },
            { "asr", "video/x-ms-asf" },
            { "asx", "video/x-ms-asf" },
            { "avi", "video/x-msvideo" },
            { "bmp", "image/bmp" },
            { "css", "text/css" },
            { "csv", "application/vnd.ms-excel" },
            { "doc", "application/msword" },
            { "flv", "video/x-flv" },
            { "gif", "image/gif" },
            { "htm", "text/html" },
            { "html", "text/html" },
            { "jpeg", "image/jpeg" },
            { "jpg", "image/jpeg" },
            { "js", "text/javascript" },
            { "mov", "video/quicktime" },
            { "mp3", "audio/mpeg" },
            { "mpa", "video/mpeg" },
            { "mpe", "video/mpeg" },
            { "mpeg", "video/mpeg" },
            { "mpg", "video/mpeg" },
            { "pdf", "application/pdf" },
            { "png", "image/png" },
            { "ppt", "application/vnd.ms-powerpoint" },
            { "pgp", "application/pgp-encrypted" },
            { "qt", "video/quicktime" },
            { "rtf", "application/msword" },
            { "swf", "application/x-shockwave-flash" },
            { "tar", "application/x-tar" },
            { "txt", "text/plain" },
            { "wav", "audio/wav" },
            { "wma", "video/x-ms-wmv" },
            { "wmv", "video/x-ms-wmv" },
            { "xls", "application/vnd.ms-excel" },
            { "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            { "xml", "text/xml" },
            { "zip", "application/x-compressed" },
            { "gz", "application/gzip-compressed" }
        };

        public static string GetMimeTypeByExtension(string extension)
        {
            string mimeType;
            if (_mimeTypes.TryGetValue(extension, out mimeType))
            {
                return mimeType;
            }

            return "application/octet-stream";
        }

        public static bool IsCompressed(string fileName)
        {
            var type = GetMimeType(fileName);
            return "application/gzip-compressed".Equals(type) || "application/x-compressed".Equals(type) || "application/pgp-encrypted".Equals(type);
        }

        public static string GetMimeType(string fileName)
        {
            string extension = Path.GetExtension(fileName).ToLower(System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(extension))
            {
                extension = extension.TrimStart('.');

                string mimeType;
                if (_mimeTypes.TryGetValue(extension, out mimeType))
                {
                    return mimeType;
                }
            }

            return "application/octet-stream";
        }
    }
}
