﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Core.Helpers
{
    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }

    }

    public class Condition : Tuple<Func<bool>, string>
    {
        public bool BreakOnInvalid { get; set; }
        public Condition(Func<bool> condition, string error, bool breakOnInvalid = false) : base(condition, error) 
        {
            BreakOnInvalid = breakOnInvalid;
        }
    }

    public class ValidatorConditions : List<Condition> 
    {
        public ValidatorConditions(List<Condition> conditions) : base(conditions){ }
    }

    public static class Validator
    {

        public static ValidationResult Validate(params Condition[] args)
        {
            return Validate(new ValidatorConditions(args.ToList()));
        }

        public static ValidationResult Validate(ValidatorConditions conditions)
        {
            var result = new ValidationResult { IsValid = true, Errors = new List<string>() };
            
            foreach (var condition in conditions)
            {
                if (condition.Item1())
                {
                    result.IsValid = false;
                    result.Errors.Add(condition.Item2);
                }
                if (condition.BreakOnInvalid)
                    break;
            }

            return result;
        }
    }
}
