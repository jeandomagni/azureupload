﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureUpload.Client.Modules
{
    public class Home2Module : NancyModule
    {
        public Home2Module()
        {
            Get["/home2/missing-files"] = parameters => {
                return View["missing-files", new { Title="Missing Files" }];
            };
        }
    }
}