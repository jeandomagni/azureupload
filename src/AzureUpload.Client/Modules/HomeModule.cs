﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureUpload.Client.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get["/"] = parameters => {
                return View["index", new { Title="Home" }];
            };

            Get["/live"] = parameters => {
                return View["live2", new { Title = "Live" }];
            };

            Get["/live2"] = parameters => {
                return View["live", new { Title = "Live" }];
            };
            
            Get["/errors"] = parameters => {
                return View["errors", new { Title = "Error Manager" }];
            };
        }
    }
}