﻿// GLOBAL SETTINGS
define([
    'underscore',
    'jquery',
    'sticky',

], function (_, $) {
    //_.templateSettings = {
    //    interpolate: /\{\{(.+?)\}\}/g
    //};


    (function ($) {

        var o = $({});

        $.subscribe = function () {
            o.on.apply(o, arguments);
        };

        $.unsubscribe = function () {
            o.off.apply(o, arguments);
        };

        $.publish = function () {
            o.trigger.apply(o, arguments);
        };

    }(jQuery));

    (function ($) {
        if ($.fn.uniqueid) return;
        var counter = 0;
        $.fn.uniqueid = function () {
            var id = 'unique-id-';
            return this.each(function () {
                if (this.getAttribute('id')) return;
                do {
                    id += counter++;
                } while (document.getElementById(id) !== null);
                this.setAttribute('id', id);
            });
        };
    }(jQuery));
});