﻿define(['jquery', 'underscore', 'chart', 'net', 'main2'], function ($, _, Chart, net) {

    //
    //
    // // Templates
    var progressTemplate = _.template($('#data-item-template').html()),
        checkboxTemplate = _.template($('#checkbox-item-template').html());




    //
    //
    // // helper functions and classes
    var readFile = function (file, fn) {
        if (!file) return;
        var reader = new FileReader();

        reader.onload = function (e) {
            fn(e.target.result);
        }

        reader.readAsText(file);
    };

    var _2d = function(num){
        return num < 10 ? '0' + num : num.toString();
    };

    var FileKey = function (client, type, date, vendor) {
        this.client = client;
        this.type = FileKey.TypeMap[type];
        this.date = new Date(date);
        this.vendor = '';
        if (this.type == 'Invoice') // only invoices are stored with a vendor prefix
            this.vendor = vendor || '';
    }

    FileKey.TypeMap = {
        'credit': 'CreditReturn',
        'invoice': 'Invoice',
        'adjustment': 'Adjustment',
        'dispense': 'Dispense',
        'qoh': 'Qoh'
    };

    FileKey.prototype = {
        toString: function () {
            var m = _2d(this.date.getMonth() + 1),
                d = _2d(this.date.getDate()),
                y = this.date.getFullYear().toString();

            return this.client + '/' + this.type + '/' + y + '/' + m + '/' + d + '/' + this.vendor;
        }
    };

    var parseFileForS3Keys = function (fileData, client) {
        var rows = fileData.split('\n').splice(1); // remove header

        // data file represents a per site model, for lookup purposes only care about the data by date and type
        // ex:
        // target/qoh/2014/07/28/*
        // target/qoh/2014/07/28/*
        // target/qoh/2014/07/28/*
        var uniqueHash = {}, keys = [];
        // Site Number, Date, Entity Type, Vendor
        for (var i = 0; i < rows.length; i++) {
            var cols = rows[i].split(','),
                date = new Date(cols[1].trim()),
                type = cols[2].trim(),
                vendor = cols[3].trim();

            var key = new FileKey(client, type, date, vendor).toString();

            if (uniqueHash.hasOwnProperty(key))
                continue;

            uniqueHash[key] = true;
            keys.push(key);
        }

        return keys;
    };



    //
    //
    // // server callback functions

    // populate dropdown
    net.on('initPackage', function (data) {
        var ddlMarkup = _.template($('#select-template').html(), {
            name: 'client-dropdown',
            defaultText: 'Choose Client',
            defaultValue: '',
            options: _.map(data.clients, function (name) {
                return { name: name, value: name };
            })
        });

        $('#choose-client').html(ddlMarkup);
    });

    // response to a request for matching keys given a missing file
    net.on('queryMatchesResponse', function (data) {
        var progress = $('div.progress'),
            results = $('form.results'),
            chooseClient = $('#choose-client');

        progress.find('.data-item:last span').text('DONE!!');
        var any = false;
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                any = true;
                var fileName = key.substr(key.lastIndexOf('/') + 1);
                results.find('#checkbox-container').append(checkboxTemplate({
                    fileName: fileName,
                    key: key,
                    size: data[key]
                }));
            }
        }

        if (any) {
            progress.empty().hide();
            chooseClient.hide();
            results.show();
        }
        else {
            $.sticky('No matching keys to reload.');
        }
    });


    //
    //
    // // Init functions to bind / setup


    var init = function () {
        // setup radio button click actions
        $('#action-form input[type=radio]').on('click', function () {
            $('#by-file,#by-key').hide();
            $('#' + this.value).toggle();
        });

        // binds the dropbox for dropping a file, populates the dropdown, etc
        initByFile();

        // binds the form for submitting a key
        initByKey();
    };


    var initByFile = function () {

        net.invoke('requestInitPackage', false);

        var enterP = false;
        // not happy with this, flag to see if the enter/leave happened on the child node or not
        $('div.dropbox p')
            .on('dragenter', function () { enterP = true; return false; })
            .on('dragleave', function () { enterP = false; return false; });

        var progress = $('div.progress'),
            results = $('form.results'),
            
            dropbox = $('div.dropbox').on('dragenter', function (e) {
                $(this).addClass('standout');
            }).on('dragleave', function (e) {
                if (!enterP) $(this).removeClass('standout');
            }).on('dragover', function (e) { // browser will load the file without stops
                e.stopPropagation();
                e.preventDefault();
            }).on('drop', function (e) {
                e.stopPropagation();
                e.preventDefault();
                dropbox.removeClass('standout');

                var dt = e.originalEvent.dataTransfer;
                var files = dt.files;
                
                dropbox.hide();
                progress.show();

                progress.append(progressTemplate({ 
                    label: "Reading File", 
                    value: '...' 
                }));
                readFile(files[0], function (text) {
                    progress.find('.data-item:last span').text('DONE!!');
                    progress.append(progressTemplate({
                        label: "Querying S3 for file candidates",
                        value: '...'
                    }));

                    var client = $('[name="client-dropdown"]').val()

                    client = 'Weis';

                    if (!client) {
                        $.sticky('Choose a client first.');
                        return;
                    }

                    var keys = parseFileForS3Keys(text, client);
                    // ignore site number because of the way most files are stored
                    net.invoke('queryReloadKeys', keys);
                });
            });

        results.on('submit', function () {
            var keys = results.find(':checkbox:checked').map(function () { return this.value; });
            if (keys.length) {
                net.invoke('requestReload', Array.prototype.join.apply(keys));
                $('#choose-client').show();
                $('#checkbox-container').empty();
                results.hide();
                dropbox.show();
            }
            else {
                $.sticky('No keys selected.');
            }
            
            return false;
        });

        $('#choose-client').on('change', 'select', function () {
            progress.empty().hide();
            dropbox.show();
        });
    };

    var initByKey = function () {
        $('#reload-key-form').on('submit', function (e) {
            var input = $(this).find('input[name="ReloadKey"]');
            var key = input.val();
            if (key) {
                net.invoke('requestReload', key);
                input.val(''); // clear
            }
            else {
                $.sticky('Invalid key');
            }
            return false;
        });
    };

    //
    //
    // // FAKE DATA FOR OS INFO

    var initGraph = function () {
        var data = {
            labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
            datasets: [
                {
                    fillColor: "rgba(38,156,217,1)",
                    strokeColor: "rgba(71,73,74,1)",
                    data: [65, 59, 90, 81, 56, 55, 40, 15, 2, 88, 5, 5, 0, 0, 15, 20, 50, 55]
                }
            ]
        }
        var ctx = $('#daily-bar-canvas').get(0).getContext('2d'),
            chart = new Chart(ctx).Bar(data, {
                scaleLineColor: "Transparent", //"#494949",
                scaleFontSize: 8,
                scaleFontFamily: "'Gotham SSm A', 'Gotham SSm B', 'Helvetica Neue', Helvetica, Calibri, Arial",
                scaleGridLineColor: 'Transparent', //#464646',
                //scaleShowLabels: false
                scaleOverride: true,
                scaleSteps: 3,
                scaleStepWidth: 25,
                barValueSpacing: 8,

            });
    };

    return {
        init: init,
        initGraph: initGraph,
    };
});