﻿define(['jquery', 'signalr.hubs'], function ($) {

    var isCreated = false;
    var createConn = function () {
        isCreated = true;
        var url = '//' + location.hostname + ':8080/signalr';

        return $.connection(url).start().done(function () {
            // upload hub is the only hub configured on the server
            var hub = $.connection.uploadHub;

            $.connection.hub.error(function () {
                console.error('An error occurred with the hub connection.');
            });

            $.publish('/data/connected', [hub]);

            hub.on('toast', function (message) {
                if (message)
                    $.sticky(message);
            });

            // seems to be a bug in CORs signalR client library that
            // the URL host in the connection is not passed through to the hub
            $.connection.hub.url = url;
            $.connection.hub.start(function () {
                console.info('INFO: Connection established.');
                $.publish('/data/hub/ready', [hub]);
            });

            $.connection.hub.disconnected(function () {
                console.error('Disconnected. Retrying in 2s...');
                setTimeout(function () {
                    $.connection.hub.start();
                }, 2000);
            });
        }).fail(function () {
            console.error('Unable to establish connection. Make sure the service is running.');
        });
    }

    $.conn = function (callbacks) {
        var conn = $.connection;
        if (!isCreated) createConn();
        $.subscribe("/data/connected", callbacks.configure);
        $.subscribe("/data/hub/ready", callbacks.ready);
        return conn;
    };
    /*
   /// USAGE
   $.conn({
       configure: function (e, hub) {
           // bind hub events
           // hub.on('someFunction', function(){});
       },
       ready: function (e, hub) {
           //invoke server functions
           // hub.server.clientConected();

       }
   });
   ///*/

    // start a hub connection
    var _hub = null, isHubReady = false, isConnected = false, waitingOn = [], waitingInvoke = [];
    var c = $.conn({
        ready: function (e, hub) {
            _hub = hub;
            isHubReady = true;
            for (var i = 0; i < waitingInvoke.length; i++) {
                hub.invoke.apply(_hub, waitingInvoke[i]);
            }
            delete waitingInvoke;
        },
        configure: function (e, hub) {
            _hub = hub;
            isConnected = true;
            for (var i = 0; i < waitingOn.length; i++) {
                hub.on.apply(_hub, waitingOn[i]);
            }
            delete waitingOn;
        },
    });
    
    // allow request to be made as soon as the module is required..
    // requests that are too early will be queued in the associated array
    // var net = require('network');
    // net.invoke('someServerFunction'); // no hub-ready callback necessary
    return {
        on: function () {
            if (isConnected)
                _hub.on.apply(_hub, arguments);
            else waitingOn.push(arguments);
        },
        invoke: function () {
            if (isHubReady)
                _hub.invoke.apply(_hub, arguments);
            else waitingInvoke.push(arguments);
        }
    };
});