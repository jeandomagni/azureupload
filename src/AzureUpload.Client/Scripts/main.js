(function ($, exports) {
    // HELPERS
    var twelveHour = function (n) {
        if (n > 12) return n - 12;
        return n;
    };

    var twoDigit = function (n, char) {
        char = char || '0';
        if (n < 10) return char + n;
        return '' + n;
    };

    // LOGGER
    var Logger = function (config) {
        var self = this;
        this.element = $(config.element);
        this.config = config;
        this.logCount = 0;
        this.namedFilters = {};
        // TODO: maybe add this on add message to remove last

        config.maxScreenSize && $(function () {
            $(window).on('resize', function () {
                var above = self.element.offset().top;
                var h = self.element.outerHeight();
                var windowH = $(this).height();

                var bodyH = $(document.body).outerHeight();

                var below = bodyH - above - h;
                self.element.height(windowH - (above + below + 42));

            }).trigger('resize');
        });
    };

    Logger.prototype.addNamedFilter = function (name, filter) {
        // clear if already exists
        if (this.namedFilters.hasOwnProperty(name))
            this.removeNamedFilter(name);
        // reset
        this.namedFilters[name] = filter;
        var f = this.filterSelector || '';
        f += f ? ',' + filter : filter;
        this.filterSelector = f;
        return this;
    };

    Logger.prototype.removeNamedFilter = function (name) {
        var namedFilter = this.namedFilters[name],
            f = this.filterSelector || '';
        f = f.replace(',' + namedFilter, '');
        f = f.replace(namedFilter, '');
        this.filterSelector = f;
        delete this.namedFilters[name];
        return this;
    };

    Logger.prototype.filter = function (selector) {
        if (selector !== undefined) {
            this.filterSelector = selector;
            for (var filter in this.namedFilters) {
                if (this.filterSelector) this.filterSelector += ',' + this.namedFilters[filter];
                else this.filterSelector = this.namedFilters[filter];
            }
        }

        var logs = this.element.find(' > span').show();
        if (this.filterSelector)
            logs.filter(this.filterSelector).hide();

        return this;
    };

    Logger.prototype.log = function (message) {
        this.addMessage('log', message);
        return this;
    };

    Logger.prototype.info = function (message) {
        this.addMessage('info', message);
        return this;
    };

    Logger.prototype.error = function (message) {
        this.addMessage('error', message);
        return this;
    };

    Logger.prototype.warn = function (message) {
        this.addMessage('warn', message);
        return this;
    };

    Logger.prototype.addMessage = function (type, message) {
        if (this.config.formatMessage) {
            message = this.config.formatMessage(message);
        }

        var d = new Date();
        var span = $('<span class="' + type + '" />')
            .append('<abbr /> ' + message);

        if (this.filterSelector && span.is(this.filterSelector))
            span.hide();

        this.element.prepend(span);

        span.find('abbr')
            .html(twoDigit(twelveHour(d.getHours()), '&nbsp;') + ':' + twoDigit(d.getMinutes()) + ':' + twoDigit(d.getSeconds()));

        this.element.scrollTop(0);
        this.logCount++;
        return this;
    };

    Logger.prototype.clear = function () {
        this.element.empty();
        this.logCount = 0;
        return this;
    };

    // binds form fields by name, or [data-bind="somekey"]
    // to a JSON or literal object values
    $.fn.dataBind = function (data) {
        return this.find('[data-bind],[name]').each(function () {
            var k = this.getAttribute('data-bind') || this.name,
                v = data[k], $this;
            if (v !== undefined) {
                var target = this.getAttribute('data-bind-target'),
                    $this = $(this);
                if (target)
                    this.setAttribute(target, v);
                else {
                    if ($this.is(':input'))
                        $this.val(v);
                    else $this.html(v)
                }
            }
        });
    };

    (function ($) {

        var o = $({});

        $.subscribe = function () {
            o.on.apply(o, arguments);
        };

        $.unsubscribe = function () {
            o.off.apply(o, arguments);
        };

        $.publish = function () {
            o.trigger.apply(o, arguments);
        };

    }(jQuery));

    (function ($) {
        if ($.fn.uniqueid) return;
        var counter = 0;
        $.fn.uniqueid = function () {
            var id = 'unique-id-';
            return this.each(function () {
                if(this.getAttribute('id')) return;
                do {
                    id += counter++;
                } while (document.getElementById(id) !== null);
                this.setAttribute('id', id);
            });
        };
    }(jQuery));

    var isCreated = false;
    var createConn = function () {
        isCreated = true;
        var url = '//' + location.hostname + ':8080/signalr';

        return $.connection(url).start().done(function () {
            // upload hub is the only hub configured on the server
            var hub = $.connection.uploadHub;

            $.connection.hub.error(function () {
                console.error('An error occurred with the hub connection.');
            });

            $.publish('/data/connected', [hub]);

            hub.on('toast', function (message) {
                if (message)
                    $.sticky(message);
            });

            // seems to be a bug in CORs signalR client library that
            // the URL host in the connection is not passed through to the hub
            $.connection.hub.url = url;
            $.connection.hub.start(function () {
                console.info('INFO: Connection established.');
                $.publish('/data/hub/ready', [hub]);
            });

            $.connection.hub.disconnected(function () {
                console.error('Disconnected. Retrying in 2s...');
                setTimeout(function () {
                    $.connection.hub.start();
                }, 2000);
            });
        }).fail(function () {
            console.error('Unable to establish connection. Make sure the service is running.');
        });
    }

    /*
    /// USAGE
    $.conn({
        configure: function (e, hub) {
            // bind hub events
            // hub.on('someFunction', function(){});
        },
        ready: function (e, hub) {
            //invoke server functions
            // hub.server.clientConected();

        }
    });
    ///*/
    $.conn = function (callbacks) {
        var conn = $.connection;
        if (!isCreated) conn = createConn();
        $.subscribe("/data/connected", callbacks.configure);
        $.subscribe("/data/hub/ready", callbacks.ready);
        return conn;
    };

    exports.Logger = Logger;

})(jQuery, this);