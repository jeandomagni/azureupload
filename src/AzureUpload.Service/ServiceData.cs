﻿using AzureUpload.Core.Models;
using AzureUpload.Service.Hubs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Service
{
    public static class ServiceData
    {
        public static dynamic GetInitPackage(bool includeActiveEvents = true)
        {
            return new
            {
                activeThreadEvents = includeActiveEvents ? GetActiveThreadEvents() : null,
                clients = ServiceData.GetClients(),
                config = ServiceData.GetConfig(),
                serviceInfo = new
                {
                    lastConfigChangeDate = Service.Instance.LastConfigChangeDate.ToString("s"),
                    lastSweepDate = Service.Instance.LastSweepDate.ToString("s")
                }
            };
        }

        public static dynamic GetErrorPackage()
        {
            var clientErrors = new Dictionary<string, object>();
            var totalErrors = 0;
            var clients = Service.Instance.Config.Clients.ToList();
            var outputRoot = Service.Instance.Config.OutputRootPath;
            foreach (var client in clients)
            {
                var errorDir = Path.Combine(outputRoot, client.Name, "Exception");
                var files = Directory.EnumerateFiles(errorDir, "*", SearchOption.AllDirectories);

                if (files.Count() == 0) continue;

                var errorFiles = files.Where(x => !x.EndsWith(".error")).ToArray();
                if (errorFiles.Length == 0) continue;
                
                totalErrors += errorFiles.Length;
                
                clientErrors[client.Name] = errorFiles.Select(path => {
                    return new {
                        path = path,
                        fileName = Path.GetFileName(path)
                    };
                });
            }

            return new { errorCount = totalErrors, errorSummary = clientErrors };
        }

        internal static dynamic GetClients()
        {
            return Service.Instance.Config.Clients.Select(x => x.Name).ToList();
        }

        internal static FileWatcherConfig GetConfig()
        {
            return Service.Instance.Config;
        }

        internal static List<QueueItemEvent> GetActiveThreadEvents()
        {
            var ordered = UploadHub.LastQueueItemEvents
                .OrderBy(x=> x.Value.EventType == EventType.BeganWork)
                .ThenByDescending(x=> x.Value.CreatedUtc)
                .ThenBy(x => x.Value.Count)
                .Select(x => x.Value)
                .ToList();
            return ordered;
        }
    }
}
