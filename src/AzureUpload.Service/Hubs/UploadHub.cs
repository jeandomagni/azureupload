﻿using AzureUpload.Core.Helpers;
using AzureUpload.Core.Models;
using AzureUpload.Data.Services;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AzureUpload.Service.Hubs
{
    [HubName("uploadHub")]
    public class UploadHub : Hub
    {
        public static IHubContext HubContext { get; set; }
        // SENDING
        
        static UploadHub() 
        {
            HubContext = GlobalHost.ConnectionManager.GetHubContext<UploadHub>();
            LastQueueItemEvents = new ConcurrentDictionary<string, QueueItemEvent>();
        }

        public static ConcurrentDictionary<string, QueueItemEvent> LastQueueItemEvents { get; private set; }

        public static void SendQueueItemEvent(QueueItemEvent data)
        {
            // if all sends are through this method tracking is easy..
            QueueItemEvent dummy;
            if ((data.Archive == false && data.QueueType == QueueType.UploadQueue && data.EventType == EventType.Removed) // when archiving is off...
                || ((data.QueueType == QueueType.ArchiveQueue || data.QueueType == QueueType.DownloadQueue) && data.EventType == EventType.Removed)) // standard procedure an id is removed after it completes queue
                LastQueueItemEvents.TryRemove(data.Id, out dummy); // cleanup on last step.
            else if(data.EventType != EventType.Error && data.QueueType == QueueType.Unknown)
                LastQueueItemEvents[data.Id] = data;

            if (data.EventType == EventType.Removed && data.QueueType != QueueType.Unknown && data.Count == 0)
            {
                // items are being left in the archive queue sometimes..
                LastQueueItemEvents = new ConcurrentDictionary<string, QueueItemEvent>(LastQueueItemEvents.Where(x => x.Value.QueueType == data.QueueType));
            }

            HubContext.Clients.All.queueItemEvent(data);
        }

        // RESPONDING
        public Task SaveRawConfig(string newConfig)
        {
            if (string.IsNullOrWhiteSpace(newConfig))
                return Clients.Caller.saveRawConfigResult(new { success = false, message = "Could not save empty configuration file." });

            var success = true;
            var additionalMessage = "";
            FileWatcherConfig config = null;
            try
            {
                var ser = new XmlSerializer(typeof(FileWatcherConfig));
                using (var sr = new StringReader(newConfig))
                    config = (FileWatcherConfig)ser.Deserialize(sr); // if this works the file could be parsed
                
                // make sure clients are unique
                if (config != null && config.Clients.Select(x => x.Name).Distinct().Count() == config.Clients.Count)
                {
                    Service.Instance.LastConfigChangeType = ConfigChangeType.Unknown;
                    File.WriteAllText(Util.Setting("ClientConfigPath"), newConfig, Encoding.Default);
                    using (var service = new FileVerificationService())
                        service.UpdateChainFileSettings(config);
                }
                else
                {
                    success = false;
                    additionalMessage = " Client nodes must be unique.";
                }
            }
            catch
            {
                success = false;
            }

            return Clients.Caller.saveRawConfigResult(new
            {
                success = success,
                message = "Could not save new configuration." + additionalMessage
            });
        }

        public Task RequestExceptionDetail(string clientName, string path, string fileName)
        {
            var config = Service.Instance.Config;
            var c = config.Clients.FirstOrDefault(x => (clientName ?? "").Equals(x.Name, StringComparison.OrdinalIgnoreCase));
            
            if (c == null)
                return Clients.Caller.toast(string.Format("Unable to locate client {0}", clientName));

            var dir = new FileInfo(path).Directory;
            var sub = dir.Name;
            var type = dir.Parent.Name;

            if (type == "Exception")
            {
                type = sub;
                sub = string.Empty;
            }

            var f = c.Files.FirstOrDefault(x => {
                return (x.FileTypeOverride ?? x.FileType.ToString()).Equals(type) && (sub.Equals(x.OutputSubFolder ?? string.Empty));
            });

            if (c == null)
                return Clients.Caller.toast(string.Format("Unable to locate file definition for path {0}", path));

            var exceptionDetailPath = Path.Combine(
                PathHelper.GetExceptionDir(Service.Instance.Config.OutputRootPath, c, f),
                fileName + ".error"
            );

            if (!File.Exists(exceptionDetailPath))
                return Clients.Caller.toast(string.Format("Error details not found for file {0}", fileName));

            var fileText = string.Empty;
            try
            {
                fileText = File.ReadAllText(exceptionDetailPath);
            }
            catch
            {
                return Clients.Caller.toast(string.Format("Unable to read file {0}", fileName));
            }

            return Clients.Caller.exceptionDetail(fileText);
        }

        public Task RequestInitPackage(bool includeActiveEvents = true)
        {
            var pkg = ServiceData.GetInitPackage(includeActiveEvents);
            return Clients.Caller.initPackage(pkg);
        }

        public Task RequestErrorSummary()
        {
            return Clients.Caller.errorSummary(ServiceData.GetErrorPackage());
        }

        public Task RequestRetryException(string clientName, string path, string fileName, bool archive)
        {
            var retryType = archive ? RetryExceptionType.Archive : RetryExceptionType.Upload;
            var c = Service.Instance.Config.Clients
                .FirstOrDefault(x => (clientName ?? string.Empty).Equals(x.Name, StringComparison.OrdinalIgnoreCase));

            if (c != null)
            {
                RetryException(c, path, fileName, retryType);
                return Clients.Caller.toast(string.Format("Retrying {0} for file {1} for {2}", retryType.ToString().ToLower(), fileName, clientName));
            }

            return Clients.Caller.toast(string.Format("Failed to find config for client: {0}", clientName));
        }

        public Task RequestRetryAllUploadExceptions()
        {
            var errorPackage = ServiceData.GetErrorPackage();
            var clientConfigLookup = Service.Instance.Config.Clients.ToDictionary(x => x.Name);

            foreach (var kvp in errorPackage.errorSummary)
            {
                string clientName = kvp.Key;
                var errorFiles = kvp.Value;

                var clientConfig = clientConfigLookup[clientName];

                foreach (var errorFile in errorFiles)
                {
                    var path = errorFile.path;
                    var fileName = errorFile.fileName;

                    RetryException(clientConfig, path, fileName, RetryExceptionType.Upload);
                }
            }

            return Clients.Caller.toast("Retrying all upload errors");
        }

        private void RetryException(ClientConfig clientConfig, string path, string fileName, RetryExceptionType retryType)
        {
            var dir = new FileInfo(path).Directory;
            var sub = dir.Name;
            var type = dir.Parent.Name;

            if (type == "Exception")
            {
                type = sub;
                sub = string.Empty;
            }

            var f = clientConfig.Files.FirstOrDefault(x => {
                return (x.FileTypeOverride ?? x.FileType.ToString()).Equals(type) && (sub.Equals(x.OutputSubFolder ?? string.Empty));
            }) ?? new FileConfig();

            Service.Instance.RetryException(clientConfig, f, fileName, retryType);
        }

        public Task RequestRawConfig()
        {
            var fileString = string.Empty;
            try
            {
                fileString = File.ReadAllText(Util.Setting("ClientConfigPath"));
            }
            catch { }
            return Clients.Caller.rawConfig(fileString);
        }

        public Task RequestSweep(string clientName)
        {
            if(Service.Instance.Config.Status == Status.Stopped)
                return Clients.Caller.toast("Cannot sweep when the service is stopped.");
            var swept = false;
            var client = "";
            if (!string.IsNullOrWhiteSpace(clientName))
            {
                var c = Service.Instance.Config.Clients
                    .FirstOrDefault(x => clientName.Equals(x.Name, StringComparison.OrdinalIgnoreCase));
                
                if (c != null)
                {
                    swept = true;
                    client = c.Name;
                    Service.Instance.SweepClient(c);
                }
            }
            else
            {
                swept = true;
                Service.Instance.Sweep();
            }

            return Clients.Caller.toast(swept ? string.Format("Sweep {0}completed successfully.", string.Empty == client ? "" : "for " + client + " ") : "");
        }

        public Task QueryReloadKeys(List<string> query)
        {
            if (Service.Instance.Config.Status == Status.Stopped)
                return Clients.Caller.toast("Cannot reload a file when the service is stopped.");

            if (query != null && query.Count > 0)
            {
                var matches = Service.ReloadQueue.QueryKeysByPatters(query);
                return Clients.Caller.queryMatchesResponse(matches);
            }

            return Clients.Caller.toast("Invalid key");

        }

        public Task RequestReload(string keys)
        {
            if (Service.Instance.Config.Status == Status.Stopped)
                return Clients.Caller.toast("Cannot reload a file when the service is stopped.");

            if (!string.IsNullOrWhiteSpace(keys))
            {
                foreach(var key in keys.Split(','))
                    Service.ReloadQueue.Enqueue(key);

                return Clients.Caller.toast("Attempting reload..");
            }
          
            return Clients.Caller.toast("Invalid key");
            
        }

        public Task RequestActiveEvents()
        {
            var events = ServiceData.GetActiveThreadEvents();
            return Clients.Caller.threadEventBatch(events);
        }

        public Task RequestClients()
        {
            var clients = ServiceData.GetClients();
            return Clients.Caller.clientList(clients);
        }
        // TODO: move this to the config change
        public Task RequestStop() 
        {
            var path = Util.Setting("ClientConfigPath");
            var success = true;
            try
            {
                var config = Util.LoadFile<FileWatcherConfig>(Util.Setting("ClientConfigPath"));
                // Set new status
                config.Status = Status.Stopped;
                Service.Instance.LastConfigChangeType = ConfigChangeType.Status;
                Util.SaveFile(path, config);
            }
            catch
            {
                success = false;
            }
            if(success)
                return Clients.All.stop();
            else
                return Clients.Caller.toast("Could not stop the service. Unable to write to the configuration file.");
        }

        public Task RequestStart()
        {
            var path = Util.Setting("ClientConfigPath");
            var isPlay = false;
            var success = true;
            try
            {
                var config = Util.LoadFile<FileWatcherConfig>(Util.Setting("ClientConfigPath"));
                isPlay = config.Status == Status.Paused;
                // Set new status
                config.Status = Status.Running;
                Service.Instance.LastConfigChangeType = ConfigChangeType.Status;
                Util.SaveFile(path, config);
            }
            catch
            {
                success = false;
            }
            if (success)
                return isPlay ? Clients.All.play() : Clients.All.start();
            else
                return Clients.Caller.toast("Could not start/resume the service. Unable to write to the configuration file.");
        }

        public Task RequestPause()
        {
            var path = Util.Setting("ClientConfigPath");
            var success = true;
            try
            {
                var config = Util.LoadFile<FileWatcherConfig>(Util.Setting("ClientConfigPath"));
                // Set new status
                config.Status = Status.Paused;
                Service.Instance.LastConfigChangeType = ConfigChangeType.Status;
                Util.SaveFile(path, config);
            }
            catch
            {
                success = false;
            }
            if (success)
                return Clients.All.pause();
            else
                return Clients.Caller.toast("Could not pause the service. Unable to write to the configuration file.");
        }
    }
}
