﻿using Amazon;
using Amazon.S3.Model;
using AzureUpload.Core.Helpers;
using AzureUpload.Core.Models;
using AzureUpload.Service.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureUpload.Service.Queues
{
    public class ReloadQueue : IDisposable
    {
        // static 
        static BlockingCollection<string> _queue = new BlockingCollection<string>();
        static HashSet<string> _queuedKeys = new HashSet<string>();
        static HashSet<string> _queuedTempFileNames = new HashSet<string>();

        private readonly string _accessKey;
        private readonly string _secretKey;
        private readonly string _bucketName;

        object locker = new object();

        public ReloadQueue(int workerCount)
        {
            _bucketName = Util.Setting("S3BucketName");
            _accessKey = Util.Setting("S3AccessKey");
            _secretKey = Util.Setting("S3SecretKey");

            var factory = new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);
            for (var i = 0; i < workerCount; i++)
                factory.StartNew(Consume);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public Dictionary<string, string> QueryKeysByPatters(IEnumerable<string> query)
        {
            using (var amazonClient = AWSClientFactory.CreateAmazonS3Client(_accessKey, _secretKey))
            {
                // EXAMPLE QUERY: "Target/Qoh/yyyy/MM/dd/"
                var matches = new Dictionary<string, string>();
                foreach (var prefix in query)
                    using (var res = amazonClient.ListObjects(new ListObjectsRequest{ BucketName = _bucketName, Prefix = prefix }))
                        if (res != null && res.S3Objects != null && res.S3Objects.Count > 0)
                            foreach (var obj in res.S3Objects)
                                if (matches.ContainsKey(obj.Key))
                                    matches[key: obj.Key] = Util.GetFileSize(obj.Size);
                                else 
                                    matches.Add(obj.Key, Util.GetFileSize(obj.Size));
                return matches;
            }
        }

        public void Enqueue(string key)
        {
            if (_queuedKeys.Contains(key)) // never queue the same key twice
                return;

            var added = _queue.TryAdd(key);
            
            if (added)
            {
                lock (locker) _queuedKeys.Add(key);
            }
        }

        void Consume()
        {
            foreach (var key in _queue.GetConsumingEnumerable())
            {
                Console.WriteLine("ReloadQueue: {0}", key);

                bool success = false, error = false;
                string tempFilePath = string.Empty;

                try
                {
                    var fileName = key.Substring(key.LastIndexOf("/") + 1);
                    tempFilePath = Service.Instance.TempDir + "\\" + fileName;
                    
                    if (_queuedTempFileNames.Contains(tempFilePath)) // handles unique file names by appending some random characters before the file extensions
                    {
                        var dotIndex = Path.GetFileName(tempFilePath).IndexOf(".");
                        if (dotIndex == -1)
                            dotIndex = tempFilePath.Length;
                        else dotIndex = Path.GetDirectoryName(tempFilePath).Length + dotIndex;
                        
                        tempFilePath = tempFilePath.Substring(0, dotIndex)
                            + "-"
                            + Path.GetFileNameWithoutExtension(Path.GetRandomFileName())
                            + tempFilePath.Substring(dotIndex);
                    }
                    
                    lock (locker) 
                        _queuedTempFileNames.Add(tempFilePath);

                    // create a file stream
                    
                    using (var amazonClient = AWSClientFactory.CreateAmazonS3Client(_accessKey, _secretKey))
                    {
                        var req = new GetObjectRequest { 
                            BucketName = _bucketName,
                            Key = key 
                        };

                        using (var res = amazonClient.GetObject(req))
                        {
                            
                            success = res.StatusCode == System.Net.HttpStatusCode.OK;
                            if (success)
                            {
                                res.WriteResponseStreamToFile(tempFilePath);
                            }
                        }
                    }

                    var clientConfig = Service.Instance.Config.Clients.FirstOrDefault(x => x.Name.Equals(key.Substring(0, key.IndexOf("/")), StringComparison.OrdinalIgnoreCase));
                    if (clientConfig == null)
                        throw new InvalidOperationException(string.Format("Cannot reload file. Invalid client in key {0}", key));

                    var keyWithoutClient = key.Substring(key.IndexOf("/") + 1);
                    var fileConfig = clientConfig.Files.Where(x=> (x.FileTypeOverride ?? x.FileType.ToString()).Equals(keyWithoutClient.Substring(0, keyWithoutClient.IndexOf("/"))));
                    if (fileConfig == null || fileConfig.Count() == 0)
                        throw new InvalidOperationException(string.Format("Cannot reload file. Invalid file type in key {0}", key));

                    if (fileConfig.Count() > 1) // possibly an invoice
                    {
                        var keyWithoutType = keyWithoutClient.Substring(keyWithoutClient.IndexOf("/") + 1);
                        fileConfig = fileConfig.Where(x => 
                            !string.IsNullOrWhiteSpace(x.OutputSubFolder) 
                                && x.OutputSubFolder.Equals(keyWithoutType.Substring(0, keyWithoutType.IndexOf("/")), StringComparison.OrdinalIgnoreCase));
                    }

                    if (fileConfig.Count() != 1)
                        throw new InvalidOperationException(string.Format("Cannot reload file. Invalid file type in key {0}", key));

                    var uploadInfo = Service.CreateUploadInfo(clientConfig, fileConfig.Single(), tempFilePath);

                    // override the archive setting for reloaded files
                    uploadInfo.Archive = false;
                    Service.UploadQueue.Enqueue(uploadInfo, (res) => {
                        if (res.Success || res.UploadInfo.Retries == Util.Setting<int>("MaxReqries"))
                            _queuedTempFileNames.Remove(res.UploadInfo.Path);
                        
                        Service.Instance.UploadResultHandler(res);
                    });
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    Service.Instance.Logger.WriteEntry("Unable to download file: " + key + Environment.NewLine + ex.ToString(), System.Diagnostics.EventLogEntryType.Warning);
                }
                finally
                {
                    if (!success)
                    {
                        try { File.Delete(tempFilePath); }
                        catch { }
                        lock (locker)
                        {
                            _queuedTempFileNames.Remove(tempFilePath);
                        }
                    }
                    _queuedKeys.Remove(key);
                }
            }
        }

        public void Dispose()
        {
            _queue.CompleteAdding();
        }
    }
}
