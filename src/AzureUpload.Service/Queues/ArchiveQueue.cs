﻿using AzureUpload.Service.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using System.Net;
using SevenZipLib;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using ProtoBuf;
using System.IO.Compression;
using AzureUpload.Core.Models;
using AzureUpload.Core.Helpers;
using AzureUpload.Data.Services;
using System.Web;
using System.Web.Caching;
using AzureUpload.Data.Models;

namespace AzureUpload.Service.Queues
{
    public class ArchiveQueue : IDisposable
    {
        class WorkItem
        {
            public Action<ArchiveResult> Callback { get; set; }
            public UploadInfo UploadInfo { get; set; }
            public ArchiveInfo ArchiveInfo { get; set; }
        }

        // static 
        static BlockingCollection<WorkItem> _queue = new BlockingCollection<WorkItem>();
        static ManualResetEvent _pauseTasks = new ManualResetEvent(true);
        static bool _stopped = false;
        static bool _paused = false;
        static readonly Regex siteExtractorRegex = new Regex(@"_?(\d+)_");

        static HashSet<string> _queuedFiles = new HashSet<string>();

        object locker = new object();
        readonly string _bucketName;
        readonly string _accessKey;
        readonly string _secretKey;
        readonly AmazonS3 client;

        readonly Dictionary<int, int> _retryTimeoutIncreaseMap = new Dictionary<int, int>{
            { 0, 3 }, // no retries gets 3 min timeout
            { 1, 6 }, // first retry gets 6 min timeout
            { 2, 10 }, // ...
            { 3, -1 } // no timeout
        };

        // Public Accessors
        public bool IsStopped { get { return _stopped; } }
        public bool IsPaused { get { return _paused; } }
        public int Count { get { return _queuedFiles.Count; } }

        public ArchiveQueue(int workerCount = 1)
        {
            // TODO: read from config
            _bucketName = Util.Setting("S3BucketName");
            _accessKey = Util.Setting("S3AccessKey");
            _secretKey = Util.Setting("S3SecretKey");

            client = Amazon.AWSClientFactory.CreateAmazonS3Client(_accessKey, _secretKey);

            if (!AmazonS3Util.DoesS3BucketExist(_bucketName, client))
            {
                client.PutBucket(new PutBucketRequest {
                    BucketName = _bucketName,
                    BucketRegion = S3Region.US
                });
            }

            var factory = new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);

            for (var i = 0; i < workerCount; i++)
                factory.StartNew(Consume);
        }

        public void Enqueue(string archivePath, UploadInfo info, Action<ArchiveResult> archiveComplete)
        {
            if (_queuedFiles.Contains(archivePath) || IsStopped) // never queue the same file twice
                return;

            var workItem = new WorkItem {
                UploadInfo = info,
                ArchiveInfo = new ArchiveInfo {
                    Path = archivePath,
                    Retries = 0
                },
                Callback = archiveComplete
            };

            var added = _queue.TryAdd(workItem);

            AfterEnqueue(added, workItem);
        }

        private void RetryEnqueue(WorkItem workItem)
        {
            if (_queuedFiles.Contains(workItem.ArchiveInfo.Path) || IsStopped) // never queue the same file twice
                return;

            bool added = false;

            if (workItem.ArchiveInfo.RetryReason == ArchiveRetryReason.FileInUse)
            {
                // seeing a file in use error
                var t = new Thread(new ThreadStart(() => {
                    Thread.Sleep(1000); // give it a second
                    added = _queue.TryAdd(workItem);
                    AfterEnqueue(added, workItem);
                }));
                t.Start();
            }
            else
            {
                added = _queue.TryAdd(workItem);
                AfterEnqueue(added, workItem);
            }

        }

        private void AfterEnqueue(bool added, WorkItem workItem)
        {
            if (added)
            {
                lock (locker) _queuedFiles.Add(workItem.ArchiveInfo.Path); // if added, store

                UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.ArchiveQueue, EventType.Added) {
                    Retries = workItem.ArchiveInfo.Retries,
                    Count = Count
                });
            }
        }

        public void Stop()
        {
            _stopped = true;
        }

        public void Start()
        {
            _stopped = false;
        }

        public void Pause()
        {
            _paused = true;
            _pauseTasks.Reset();
        }

        public void Resume()
        {
            _paused = false;
            _pauseTasks.Set();
        }

        // TODO REMOVE
        public enum FIleArchiveStorageType
        {
            Unknown,
            BySite,
            BySiteGrouped,
            ByTimeZone,
        }

        private static T GetCached<T>(string key, TimeSpan timeout, Func<T> getDirect)
        {
            var cache = HttpRuntime.Cache;
            object valueCached = cache[key];
            if (valueCached != null)
            {
                return (T)valueCached;
            }
            else
            {
                T valueDirect = getDirect();
                cache.Add(key, valueDirect, null, DateTime.Now.AddDays(1), new TimeSpan(), CacheItemPriority.Default, null);
                return valueDirect;
            }
        }

        private Dictionary<string, int> GetSites(int chainId)
        {
            var sites = GetCached<Dictionary<string, int>>(string.Format("GetSites/{0}", chainId), new TimeSpan(24, 0, 0), () => {
                using (var siteService = new SiteService())
                    return siteService.GetSites(chainId).ToDictionary(x => x.NPI, x => x.SiteNumber);
            });

            return sites;
        }

        private FileVerificationSetting GetChainFileSetting(int chainId)
        {
            var settings = GetChainFileSettings();
            return settings.ContainsKey(chainId) ? settings[chainId] : null;
        }

        private Dictionary<int, FileVerificationSetting> GetChainFileSettings()
        {
            var settings = GetCached<Dictionary<int, FileVerificationSetting>>("GetChainFileSettings", new TimeSpan(24, 0, 0), () => {
                using (var fileService = new FileVerificationService())
                    return fileService.GetChainFileSettings().ToDictionary(x => x.ChainId);
            });

            return settings;
        }

        private static int GetSiteNumberFromFileName(string fileName)
        {
            var match = siteExtractorRegex.Match(fileName);
            int num = 0;

            if (match.Groups[1].Success)
                int.TryParse(match.Groups[1].Value, out num);

            return num;
        }

        void Consume()
        {
            foreach (WorkItem workItem in _queue.GetConsumingEnumerable())
            {
                if (_stopped)
                {
                    lock (locker) _queuedFiles.Remove(workItem.ArchiveInfo.Path);
                    continue;
                }
                if (_paused)
                    _pauseTasks.WaitOne();

                if (_stopped)
                {
                    lock (locker) _queuedFiles.Remove(workItem.ArchiveInfo.Path);
                    continue;
                }

                UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.ArchiveQueue, EventType.BeganWork) {
                    Retries = workItem.ArchiveInfo.Retries,
                    Count = Count
                });

                var archiveResult = new ArchiveResult {
                    Path = workItem.ArchiveInfo.Path,
                    UploadInfo = workItem.UploadInfo
                };

                SevenZipArchive archive = null;
                FileStream file = null;

                try
                {

                    var req = new PutObjectRequest {
                        BucketName = _bucketName,
                        Key = GetArchiveKey(workItem.UploadInfo, workItem.ArchiveInfo.Path),
                        Timeout = _retryTimeoutIncreaseMap[0] * 60 * 1000
                    };

                    Console.WriteLine("ArchiveQueue: {0}", workItem.UploadInfo.Path);

                    if (workItem.ArchiveInfo.Retries > 0 && workItem.ArchiveInfo.RetryReason == ArchiveRetryReason.Timeout)
                    {
                        req.Timeout = _retryTimeoutIncreaseMap[workItem.ArchiveInfo.Retries] * 60 * 1000;
                        workItem.ArchiveInfo.RetryReason = ArchiveRetryReason.Unknown; // reset
                    }

                    file = File.OpenRead(workItem.ArchiveInfo.Path);
                    var checksum = AmazonS3Util.GenerateChecksumForStream(file, false);

                    string tempFilePath = string.Empty;
                    try // less than essentail consume partial exception
                    {
                        // TODO: null checks
                        var clientInfo = Service.Instance.Config.Clients.FirstOrDefault(x => x.Name == workItem.UploadInfo.ClientName);
                        
                        var uniqueSiteNumberHash = new HashSet<int>();
                        var meta = new FileMeta {
                            SiteNumbers = new List<int>(),
                            Sizes = new List<int>()
                        };

                        if (req.Key.IsPackaged() || req.Key.GetSiteNumber() < 1) // packaged or undecipherable site number
                        {
                            archive = new SevenZipArchive(file);
                            var npiLookup = GetSites(clientInfo.ChainId);
                            foreach (var x in archive)
                            {
                                int siteNum;
                                if (!x.IsUntitled && (siteNum = x.FileName.GetSiteNumber()) > 0) // get from file name
                                {
                                    uniqueSiteNumberHash.Add(siteNum);
                                    meta.SiteNumbers.Add(siteNum);
                                }
                                else if(npiLookup.Count > 0) // get from file contents
                                {
                                    tempFilePath = Path.Combine(Service.Instance.TempDir, x.IsUntitled ? Guid.NewGuid().ToString() : x.FileName);
                                    using (var fs = File.Create(tempFilePath))
                                        x.Extract(fs);

                                    using (var sr = new StreamReader(tempFilePath))
                                    {
                                        string fileLine;
                                        string npi = string.Empty;
                                        while ((fileLine = sr.ReadLine()) != null && fileLine.Length >= 10)
                                        {
                                            var currentNpi = fileLine.Substring(0, 10);
                                            if (currentNpi == npi) continue;

                                            npi = currentNpi;
                                            var siteNumLookup = 0;

                                            npiLookup.TryGetValue(npi, out siteNumLookup);
                                            if (siteNumLookup != 0 && !uniqueSiteNumberHash.Contains(siteNumLookup))
                                            {
                                                uniqueSiteNumberHash.Add(siteNumLookup);
                                                meta.SiteNumbers.Add(siteNumLookup);
                                            }
                                        }
                                    }

                                    try { File.Delete(tempFilePath); } // Delete each file after done reading - possible naming collision here.
                                    catch { }

                                }
                            }

                            if (meta.SiteNumbers.Count > 0)
                            {
                                string output = string.Empty;
                                using (var ms = new MemoryStream())
                                {
                                    Serializer.Serialize(ms, meta);
                                    using (var msOut = new MemoryStream())
                                    {
                                        using (var cs = new DeflateStream(msOut, CompressionMode.Compress))
                                        {
                                            ms.Position = 0;
                                            ms.CopyTo(cs);
                                        }

                                        output = Convert.ToBase64String(msOut.ToArray());
                                    }
                                }

                                // can get a total of 4KB out of the headers.. 
                                var storageKey1Size = 2032; // 2KB "x-amz-meta-files": 						2048 - 16 bytes (key) 			= 2032
                                var storageKey2Size = 2047; // 2KB "x-amz-website-redirect-location": 		2048 - 1 byte (starts w '/') 	= 2047
                                var availableMetaBytes = storageKey1Size + storageKey2Size;

                                if (output.Length > availableMetaBytes) // too big for both keys
                                {
                                    Service.Instance.Logger.WriteEntry("Could not write object meta beause it was too big." + Environment.NewLine + output, System.Diagnostics.EventLogEntryType.FailureAudit);
                                }
                                else if (output.Length > storageKey1Size) // too big for one key
                                {
                                    req.AddHeader("x-amz-meta-files", output.Substring(0, storageKey1Size));
                                    req.AddHeader("x-amz-website-redirect-location", "/" + output.Substring(storageKey1Size)); // amazon validates this key starts with a /
                                }
                                else
                                {
                                    req.AddHeader("x-amz-meta-files", output);
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Service.Instance.Logger.WriteEntry("Could not write object meta." + Environment.NewLine + ex.ToString(), System.Diagnostics.EventLogEntryType.Warning);
                    }

                    if (!file.CanSeek) // possible SevenZipArchive failed and closed the stream
                    {
                        file.Close();
                        file = File.OpenRead(workItem.ArchiveInfo.Path);
                    }

                    file.Seek(0, SeekOrigin.Begin);
                    req.InputStream = file;
                    var res = client.PutObject(req);

                    var isValid = !string.IsNullOrWhiteSpace(res.ETag) && checksum.Equals(res.ETag.Replace("\"", ""), StringComparison.OrdinalIgnoreCase);
                    if (isValid)
                    {
                        archiveResult.Success = true;
                        workItem.Callback(archiveResult);
                    }
                }
                catch (Exception ex)
                {
                    if (ex is WebException)
                    {
                        var webEx = (WebException)ex;
                        if (webEx.Status == WebExceptionStatus.Timeout || webEx.Status == WebExceptionStatus.RequestCanceled)
                        {
                            // increase time limit for upload
                            workItem.ArchiveInfo.RetryReason = ArchiveRetryReason.Timeout;
                        }
                    }
                    else if (ex is IOException)
                    {
                        workItem.ArchiveInfo.RetryReason = ArchiveRetryReason.FileInUse;
                    }
                    archiveResult.Exception = ex;
                }
                finally
                {
                    lock (locker) _queuedFiles.Remove(workItem.ArchiveInfo.Path);

                    if (archive != null)
                        archive.Dispose();
                    if (file != null)
                        file.Dispose();

                    UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.ArchiveQueue, EventType.Removed) {
                        Count = Count
                    });

                    if (!archiveResult.Success)
                    {
                        if (workItem.ArchiveInfo.Retries < 3) // S3 has a 3 attempt max
                        {
                            workItem.ArchiveInfo.Retries += 1;
                            RetryEnqueue(workItem);
                        }
                        else
                        {
                            workItem.Callback(archiveResult);
                        }
                    }
                }
            }
        }

        private string GetArchiveKey(UploadInfo info, string archivePath)
        {
            var created = File.GetCreationTimeUtc(archivePath);
            if (created == DateTime.MinValue)
                created = DateTime.UtcNow;
            var name = Path.GetFileName(archivePath).ToLower();
            return string.Format("{0}/{1}/{2}/{3}",
                info.ClientName,
                info.FileTypeOverride ?? info.FileType.ToString(),
                created.ToString("yyyy/MM/dd"),
                string.IsNullOrWhiteSpace(info.OutputSubFolder) ? name : info.OutputSubFolder + "/" + name
            );
        }

        public void Dispose()
        {
            _queue.CompleteAdding();
        }
    }
}
