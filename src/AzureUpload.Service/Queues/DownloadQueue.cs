﻿using AzureUpload.Core.Helpers;
using AzureUpload.Core.Models;
using AzureUpload.Service.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureUpload.Service.Queues
{
    public class DownloadQueue : IDisposable
    {
        class WorkItem
        {
            public Action<UploadInfo> Callback { get; set; }
            public UploadInfo UploadInfo { get; set; }
            public DownloadInfo DownloadInfo { get; set; }
        }

        // static 
        static BlockingCollection<WorkItem> _queue = new BlockingCollection<WorkItem>();
        static bool _stopped = false;
        static HashSet<string> _queuedFiles = new HashSet<string>();

        object locker = new object();
        const int SLEEP_MS = 50;
        const int SLEEP_RETRY_MULTIPLIER = 8;

        public DownloadQueue()
        {
            var factory = new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);
            factory.StartNew(Consume);
        }

        public void Enqueue(UploadInfo info, Action<UploadInfo> fileReady)
        {
            if (_queuedFiles.Contains(info.Path)) // never queue the same file twice
                return;

            var added = _queue.TryAdd(new WorkItem {
                UploadInfo = info,
                DownloadInfo = new DownloadInfo{
                    Retries = 0
                },
                Callback = fileReady
            });
            
            if (added)
            {
                lock (locker) _queuedFiles.Add(info.Path);
                UploadHub.SendQueueItemEvent(new QueueItemEvent(info, QueueType.DownloadQueue, EventType.Added));
            }
        }

        void Consume()
        {
            foreach (WorkItem workItem in _queue.GetConsumingEnumerable())
            {
                if (_stopped)
                {
                    lock (locker) _queuedFiles.Remove(workItem.UploadInfo.Path);
                    continue;
                }

                Thread.Sleep(SLEEP_MS);

                Console.WriteLine("DownloadQueue: {0}", workItem.UploadInfo.Path);

                bool success = false, error = false;
                // check info and see if a file read should be tried, otherwise re-queue file
                if (workItem.DownloadInfo.Retries > 0 && workItem.DownloadInfo.LastReadAttemptUtc.HasValue)
                {
                    var lastRead = workItem.DownloadInfo.LastReadAttemptUtc.Value;
                    var nextRead = lastRead.AddMilliseconds(SLEEP_MS * workItem.DownloadInfo.Retries * SLEEP_RETRY_MULTIPLIER);
                    if (DateTime.UtcNow <= nextRead)
                    {
                        _queue.TryAdd(workItem);
                        continue;
                    }
                }
                // manually update the file size to show progress
                workItem.UploadInfo.FileSize = Util.GetFileSize(workItem.UploadInfo.Path);
                UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.DownloadQueue, EventType.BeganWork));

                try
                {
                    using (Stream stream = new FileStream(workItem.UploadInfo.Path, FileMode.Open, FileAccess.ReadWrite)) { }
                    // callback success
                    success = true;
                    workItem.Callback(workItem.UploadInfo);
                }
                catch (FileNotFoundException ex)
                {
                    error = true;
                }
                catch (IOException) // file in use while incoming
                {
                    workItem.DownloadInfo.Retries++;
                    workItem.DownloadInfo.LastReadAttemptUtc = DateTime.UtcNow;
                    _queue.TryAdd(workItem);
                }
                catch (Exception ex)
                {
                    // Seems to be a case where the file is queued for download and then does not exist.
                    // This could be on the client side then stopping the file, or a failed send. Disgard and do not keep trying.
                    error = true;
                    Service.Instance.Logger.WriteEntry("Unable to download file: " + Environment.NewLine + ex.ToString(), System.Diagnostics.EventLogEntryType.Warning);
                }
                finally
                {
                    lock (locker) _queuedFiles.Remove(workItem.UploadInfo.Path);

                    if (success || error)
                    {
                        UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.DownloadQueue, EventType.Removed) { Count = _queuedFiles.Count });
                    }
                }
            }
        }

        public void Dispose()
        {
            _queue.CompleteAdding();
        }
    }
}
