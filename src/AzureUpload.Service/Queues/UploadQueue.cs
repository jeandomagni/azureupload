﻿using AzureUpload.Core.Helpers;
using AzureUpload.Core.Models;
using AzureUpload.Service.Hubs;
using ICSharpCode.SharpZipLib.GZip;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureUpload.Service.Queues
{
    public class UploadQueue : IDisposable
    {
        class WorkItem
        {
            public Action<UploadResult> Callback { get; set; }
            public UploadInfo UploadInfo { get; set; }
            public CancellationToken? CancelToken { get; set; }
        }

        // static 
        static BlockingCollection<WorkItem> _queue = new BlockingCollection<WorkItem>();
        
        //[ThreadStatic]
        static HashSet<string> _queuedFiles = new HashSet<string>();
        static ManualResetEvent _pauseTasks = new ManualResetEvent(true);
        static bool _paused = false;
        static bool _stopped = false;
        
        public readonly bool TestMode;
        object locker = new object();


        // private readonly
        private readonly CloudStorageAccount _account;
        private readonly CloudBlobClient _blobClient;
        private readonly CloudQueueClient _cloudQueueClient;
        private readonly IHubContext HubContext;
        

        // Public Accessors
        public bool IsStopped { get { return _stopped; } }
        public bool IsPaused { get { return _paused; } }
        public int Count { get { return _queuedFiles.Count; } }

        public UploadQueue(int workerCount)
        {
            TestMode = Util.Setting<bool>("TestMode");

            if (!TestMode)
            {
                _account = Util.Setting<bool>("UseDevelopmentStorage") 
                    ? CloudStorageAccount.DevelopmentStorageAccount 
                    : new CloudStorageAccount(new StorageCredentials(Util.Setting("AccountName"), Util.Setting("SharedAccountKey")), true);

                _blobClient = _account.CreateCloudBlobClient();
                _cloudQueueClient = _account.CreateCloudQueueClient();
            }
            HubContext = GlobalHost.ConnectionManager.GetHubContext<UploadHub>();

            var factory = new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);

            for (int i = 0; i < workerCount; i++)
            {
                var t = factory.StartNew(Consume);
            }
        }

        public void Enqueue(UploadInfo info, Action<UploadResult> callback, CancellationToken? cancelToken = null)
        {
            if (_queuedFiles.Contains(info.Path) || IsStopped) // never queue the same file twice
                return;

            var added = _queue.TryAdd(new WorkItem {
                UploadInfo = info,
                Callback = callback,
                CancelToken = cancelToken
            });

            if (added)
            {
                lock (locker) _queuedFiles.Add(info.Path); // if added, store
                if (!_stopped) UploadHub.SendQueueItemEvent(new QueueItemEvent(info, QueueType.UploadQueue, EventType.Added) {
                    Count = Count
                });
            }


        }

        public void Stop()
        {
            _stopped = true;
        }

        public void Start()
        {
            _stopped = false;
        }

        public void Pause()
        {
            _paused = true;
            _pauseTasks.Reset();
        }

        public void Resume()
        {
            _paused = false;
            _pauseTasks.Set();
        }

        void Consume()
        {
            foreach (WorkItem workItem in _queue.GetConsumingEnumerable())
            {
                // stopped checks before and after pause in the event of a pause + stop
                if (_stopped)
                {
                    lock (locker) _queuedFiles.Remove(workItem.UploadInfo.Path);
                    continue;
                }

                if (_paused)
                {
                    // If we are paused, wait for the signal to indicate that
                    // we can continue
                    _pauseTasks.WaitOne();
                }

                if (_stopped)
                {
                    lock (locker) _queuedFiles.Remove(workItem.UploadInfo.Path);
                    continue;
                }

                Console.WriteLine("UploadQueue: {0}", workItem.UploadInfo.Path);
                UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.UploadQueue, EventType.BeganWork) { 
                    Count = Count
                });

                try
                {
                    string newBlobUri = null,
                            queueMessage = null,
                            tempFilePath = null,
                            fileName = Path.GetFileName(workItem.UploadInfo.Path);

                    var needsCompression = !MimeTypes.IsCompressed(fileName);
                    if (needsCompression)
                        fileName += ".gz";

                    var cloudQueue = _cloudQueueClient.GetQueueReference(workItem.UploadInfo.QueueName);
                    cloudQueue.CreateIfNotExists();

                    var container = _blobClient.GetContainerReference(workItem.UploadInfo.BlobContainer);
                    var blob = container.GetBlockBlobReference(fileName);
                    if (!TestMode)
                    {
                        using (var fileStream = File.OpenRead(workItem.UploadInfo.Path))
                        {
                            // compress if not already compressed
                            if (needsCompression)
                            {
                                tempFilePath = Path.Combine(Service.Instance.TempDir, fileName);
                                using (var tempFile = File.Create(tempFilePath))
                                {
                                    using (var gzipStream = new GZipOutputStream(tempFile))
                                    {
                                        gzipStream.IsStreamOwner = false;
                                        fileStream.CopyTo(gzipStream);
                                    }
                                    tempFile.Seek(0, SeekOrigin.Begin);
                                    workItem.UploadInfo.FileSize = Util.GetFileSize(tempFilePath);
                                    // update size on UI for queue
                                    UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.UploadQueue, EventType.BeganWork) {
                                        Count = Count
                                    });
                                    blob.UploadFromStream(tempFile);
                                }
                            }
                            else
                            {
                                blob.UploadFromStream(fileStream);
                            }
                        }

                        blob.Properties.ContentType = MimeTypes.GetMimeType(fileName);
                        newBlobUri = blob.Uri.ToString();

                        queueMessage = newBlobUri;
                        if (!string.IsNullOrWhiteSpace(workItem.UploadInfo.WorkerType))
                            queueMessage = workItem.UploadInfo.WorkerType + Environment.NewLine + newBlobUri;

                        cloudQueue.AddMessage(new CloudQueueMessage(queueMessage));
                    }

                    var result = new UploadResult {
                        Success = true,
                        BlobUri = newBlobUri,
                        WorkerMessage = queueMessage,
                        UploadInfo = workItem.UploadInfo
                    };

                    if (tempFilePath != null)
                    {
                        result.CompressionInfo = new CompressionInfo {
                            FileName = fileName,
                            TempPath = tempFilePath
                        };
                    }

                    // callback success
                    workItem.Callback(result);
                }
                catch (Exception ex)
                {
                    _queuedFiles.Remove(workItem.UploadInfo.Path);

                    workItem.Callback(new UploadResult {
                        Success = false,
                        UploadInfo = workItem.UploadInfo,
                        Exception = ex
                    });
                }
                finally
                {
                    lock (locker) _queuedFiles.Remove(workItem.UploadInfo.Path);

                    UploadHub.SendQueueItemEvent(new QueueItemEvent(workItem.UploadInfo, QueueType.UploadQueue, EventType.Removed) {
                        Count = Count
                    });
                }
            }
        }

        public void Dispose()
        {
            _queue.CompleteAdding();
        }

        internal bool HasFile(string path)
        {
            return _queuedFiles.Contains(path);
        }
    }
}
