﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.IO;
using Microsoft.AspNet.SignalR;
using Owin;
using System.Xml.Serialization;
using Microsoft.Owin.Hosting;
using AzureUpload.Service.Hubs;
using AzureUpload.Service.Queues;
using System.Threading;
using YamlDotNet.RepresentationModel.Serialization;
using System.Diagnostics;
using System.Net;
using AzureUpload.Core.Models;
using AzureUpload.Core.Helpers;
using System.Reflection;
using Microsoft.Owin;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.Cors;

// [assembly: OwinStartup(typeof(AzureUpload.Service.Service.Startup))]

namespace AzureUpload.Service
{
    public class Service : ServiceBase
    {
        static readonly object _locker = new object();

        private IDisposable WebApp;
        private IHubContext HubContext;
        private List<FileSystemWatcher> Watchers;
        private FileSystemWatcher ConfigWatcher;
        private static Service _service;

        public string TempDir { get; private set; }

        internal static Service Instance
        {
            get
            {
                lock (_locker)
                {
                    return _service;
                }
            }
            private set
            {
                lock (_locker)
                {
                    _service = value;
                }
            }
        }

        internal static UploadQueue UploadQueue;
        internal static DownloadQueue DownloadQueue;
        internal static ArchiveQueue ArchiveQueue;
        internal static ReloadQueue ReloadQueue;

        internal FileWatcherConfig Config { get; set; }
        internal DateTime LastSweepDate { get; set; }
        internal DateTime LastConfigChangeDate
        {
            get
            {
                var lastWriteUtc = File.GetLastWriteTimeUtc(Util.Setting("ClientConfigPath"));
                return TimeZoneInfo.ConvertTime(lastWriteUtc, TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"));
            }
        }
        internal ConfigChangeType LastConfigChangeType { get; set; }

        public EventLog Logger { get; set; }
        public static readonly string LogSource = "AzureFileUploader";
        public static readonly string LogName = "AzureFileUploaderLog";

        public class Startup
        {
            public void Configuration(IAppBuilder app)
            {
                app.UseCors(CorsOptions.AllowAll);
                app.MapSignalR();
            }
        }

        public void DebuggerStart()
        {
            OnStart(null);
            Console.WriteLine("Watching configuration file: {0}", Util.Setting("ClientConfigPath"));
        }

        protected override void OnStart(string[] args)
        {
            ServicePointManager.DefaultConnectionLimit = 100;
            
            Logger.WriteEntry(string.Format("Starting Azure Upload Service for config: {0}",
                Util.Setting("ClientConfigPath")), EventLogEntryType.Information);

            // create temp directory
            var programDir = Path.GetDirectoryName(this.GetType().Assembly.Location);

            TempDir = Path.Combine(programDir, "temp");

            if (!Directory.Exists(TempDir))
                Directory.CreateDirectory(TempDir);


            // make instance variable available
            Instance = this;

            // initialize
            Watchers = new List<FileSystemWatcher>();
            try
            {
                // waits for files to finish downloading to the ftp server
                DownloadQueue = new DownloadQueue();

                // sends files to azure and dropps a queue message
                UploadQueue = new UploadQueue(workerCount: Util.Setting<int>("MaxThreads"));

                // sends files to S3 and deletes the local copy upon success
                ArchiveQueue = new ArchiveQueue(workerCount: 3);

                // takes in S3 keys and downloads files into the appropriate drop location on the FTP server for reloading archived files
                ReloadQueue = new ReloadQueue(workerCount: 2);

                // loads and validates config.xml
                LoadConfigurationFile();

                // initialize the web server and signal R hubs
                CreateBroadcast();

                // called when things are stop/started or configuration changes
                Init();

                // looks for changes to the configuration file and handles re-init
                WatchConfigurationFile();

                base.OnStart(args);
            }
            catch (Exception e)
            {
                Console.WriteLine("OOPS! Something went wrong in initilization.");
                Logger.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }

            // TODO: make a sweep schedule?
        }

        #region Configuration File

        public void LoadConfigurationFile()
        {
            FileWatcherConfig config = null;
            try
            {
                config = Util.LoadFile<FileWatcherConfig>(Util.Setting("ClientConfigPath"));
            }
            catch (Exception e)
            {
                Logger.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }

            // Validate the config does not have duplicate clients or invalid values
            var result = Validator.Validate(
                new Condition(() => config == null, "Error parsing config.xml.", breakOnInvalid: true),
                new Condition(() => config.Clients.Select(x => x.Name).Distinct().Count() != config.Clients.Count, "Error parsing config.xml, clients must be unique.")
            );

            if (!result.IsValid)
                throw new InvalidOperationException(string.Join(",", result.Errors));
            else
                Config = config;

        }

        public void WatchConfigurationFile()
        {
            if (ConfigWatcher != null)
                ConfigWatcher.Dispose();

            var configPath = Util.Setting("ClientConfigPath");

            ConfigWatcher = new FileSystemWatcher(Path.GetDirectoryName(configPath), Path.GetFileName(configPath));
            ConfigWatcher.NotifyFilter = NotifyFilters.LastWrite;
            var tempDate = DateTime.MinValue;
            ConfigWatcher.Changed += (source, e) => {
                // duplicate event
                if (tempDate == LastConfigChangeDate) return;

                // writing the file may perform multiple change events. Only process if the change date is different
                LoadConfigurationFile();

                if (LastConfigChangeType != ConfigChangeType.Status)
                {
                    UploadQueue.Stop(); // dequeue items 
                    ArchiveQueue.Stop(); // dequeue items 
                }

                Init();

                var initPackage = ServiceData.GetInitPackage();
                HubContext.Clients.All.initPackage(initPackage);

                tempDate = LastConfigChangeDate;
                LastConfigChangeType = ConfigChangeType.Unknown;
            };

            ConfigWatcher.EnableRaisingEvents = true;
        }

        #endregion

        private void Init()
        {
            var wasPaused = UploadQueue.IsPaused;
            var wasStopped = UploadQueue.IsStopped;

            if (Config.Status == Status.Running)
            {
                SafeCreateDirectories();

                // resume queue 
                if (ArchiveQueue.IsStopped) ArchiveQueue.Start();
                if (ArchiveQueue.IsPaused) ArchiveQueue.Resume();
                if (UploadQueue.IsStopped) UploadQueue.Start();
                if (UploadQueue.IsPaused) UploadQueue.Resume();

                // create 
                if (wasStopped || !(LastConfigChangeType == ConfigChangeType.Status && wasPaused))
                {
                    // starts watching folders for creation or renamed files that match
                    // the file filter pattern
                    CreateWatchers();
                }

                // in the event of a pause / resume + change config
                if (wasStopped || !wasPaused)
                {
                    // scans for existing files that have not been processedthis.configWatcher
                    // a) from the service being stopped
                    // b) from missed files that were not handled
                    Sweep();
                }
            }
            else if (Config.Status == Status.Stopped)
            {
                foreach (var watcher in Watchers)
                    watcher.Dispose();

                UploadQueue.Stop();
                ArchiveQueue.Stop();
                // the threads are all waiting if it was paused.
                // resuming after stopping allow the blocking collection to be emptied
                if (wasPaused) {
                    ArchiveQueue.Resume();
                    UploadQueue.Resume();
                }
            }
            else if (Config.Status == Status.Paused)
            {
                UploadQueue.Pause();
                ArchiveQueue.Pause();
            }
        }

        private void SafeCreateDirectories()
        {
            if (!Directory.Exists(Config.OutputRootPath))
                Directory.CreateDirectory(Config.OutputRootPath);

            foreach (var client in Config.Clients)
            {
                var clientDir = Path.Combine(Config.OutputRootPath, client.Name);
                var archiveDir = Path.Combine(clientDir, "Archive");
                var exceptionDir = Path.Combine(clientDir, "Exception");
                var dropDir = client.DropFolderPath;
                if (!string.IsNullOrWhiteSpace(dropDir) && !Directory.Exists(dropDir))
                {
                    //TODO: warn that the folder did not exist in case of typo in configuration file
                    Directory.CreateDirectory(dropDir);
                }

                if (!Directory.Exists(clientDir))
                    Directory.CreateDirectory(clientDir);

                if (!Directory.Exists(archiveDir))
                    Directory.CreateDirectory(archiveDir);

                if (!Directory.Exists(exceptionDir))
                    Directory.CreateDirectory(exceptionDir);

                foreach (var file in client.Files)
                {
                    var fileArchiveDir = Path.Combine(file.ArchiveRootOverride ?? archiveDir, file.FileTypeOverride ?? file.FileType.ToString());
                    if (!Directory.Exists(fileArchiveDir))
                        Directory.CreateDirectory(fileArchiveDir);

                    var fileExceptionDir = Path.Combine(exceptionDir, file.FileTypeOverride ?? file.FileType.ToString());
                    if (!Directory.Exists(fileExceptionDir))
                        Directory.CreateDirectory(fileExceptionDir);

                    if (!string.IsNullOrWhiteSpace(file.OutputSubFolder))
                    {
                        var fileArchiveSubDir = Path.Combine(fileArchiveDir, file.OutputSubFolder);
                        if (!Directory.Exists(fileArchiveSubDir))
                            Directory.CreateDirectory(fileArchiveSubDir);

                        var fileExceptionSubDir = Path.Combine(fileExceptionDir, file.OutputSubFolder);
                        if (!Directory.Exists(fileExceptionSubDir))
                            Directory.CreateDirectory(fileExceptionSubDir);
                    }

                    if (!string.IsNullOrWhiteSpace(file.DropFolderPathOverride))
                    {
                        if (!Directory.Exists(file.DropFolderPathOverride))
                            Directory.CreateDirectory(file.DropFolderPathOverride);
                    }
                }
            }
        }

        private void CreateBroadcast()
        {
            var https = Util.Setting<bool>("UseHttps");
            var protocol = https ? "https" : "http";

            string url = protocol + "://+:8080/";


            WebApp = Microsoft.Owin.Hosting.WebApp.Start<Startup>(url);
            HubContext = GlobalHost.ConnectionManager.GetHubContext<UploadHub>();

            Logger.WriteEntry("Hub broadcasting at " + url, EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            if (Watchers != null)
                foreach (var item in Watchers)
                    item.Dispose();

            if (ConfigWatcher != null)
                ConfigWatcher.Dispose();

            if (DownloadQueue != null)
                DownloadQueue.Dispose();

            if (UploadQueue != null)
                UploadQueue.Dispose();

            if (ArchiveQueue != null)
                ArchiveQueue.Dispose();

            if (WebApp != null)
                WebApp.Dispose();

            Logger.WriteEntry("Service Stoped.", EventLogEntryType.Information);

            base.OnStop();
        }

        #region Sweeping / Watching

        public void Sweep()
        {
            if (Config.Status == Status.Stopped)
                return;

            LastSweepDate = DateTime.Now;

            foreach (var client in Config.Clients)
            {
                SweepClient(client);
            }
        }

        public void SweepClient(ClientConfig client)
        {
            if (client.Status == Status.Stopped)
                return;

            foreach (var file in client.Files)
            {
                // folder will exist unless names / folders have changed
                var dir = file.DropFolderPathOverride ?? client.DropFolderPath;
                try
                {
                    // queue any existing files that match the search patern
                    var files = Directory.EnumerateFiles(dir, file.FilterPattern);
                    foreach (var path in files)
                    {
                        var info = CreateUploadInfo(client, file, path);
                        // save enqueue - will check for queued files before adding
                        DownloadQueue.Enqueue(info, fileReady: (i) => {
                            if (file.FileType == FileType.Delete)
                                Delete(info.Path);
                            else if (!file.Upload)
                                Archive(new UploadResult { // skip upload, move file to archive location
                                    UploadInfo = i, 
                                    Success = true 
                                });
                            else
                                UploadQueue.Enqueue(i, UploadResultHandler);
                        });
                    }
                    if (!file.Archive) continue;
                    // now look for files in the archive folder and move those to the archive queue..
                    var archivePath = PathHelper.GetArchiveDir(Service.Instance.Config.OutputRootPath, client, file);

                    files = Directory.EnumerateFiles(archivePath);
                    foreach (var path in files)
                    {
                        var info = CreateUploadInfo(client, file, path);
                        ArchiveQueue.Enqueue(path, info, ArchiveResultHandler);
                    }
                }
                catch (Exception e)
                {
                    Logger.WriteEntry(e.ToString(), EventLogEntryType.Error);
                }
            }
        }

        private void CreateWatchers()
        {
            if (Config.Status == Status.Stopped) // no watching while stopped
                return;

            if (Watchers.Count > 0)
            {
                Watchers.ForEach(x => x.Dispose());
                Watchers = new List<FileSystemWatcher>();
            }
            FileSystemEventHandler errorHandler = (source, e) => {
                // send error package to subscribed clients
                if (e.Name.EndsWith(".error"))
                    return; //  ignore the error files that are created

                var errorPackage = ServiceData.GetErrorPackage();
                HubContext.Clients.All.errorSummary(errorPackage);
            };

            var uniqueWatcherHash = new HashSet<string>();

            foreach (var client in Config.Clients)
            {
                if (client.Status == Status.Stopped)
                    continue;

                // watch error folder
                var exceptionFolder = Path.Combine(Config.OutputRootPath, client.Name, "Exception");

                var errorWatcher = new FileSystemWatcher(exceptionFolder, "*.*") {
                    NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.FileName,
                    IncludeSubdirectories = true
                };

                errorWatcher.Created += errorHandler;
                errorWatcher.Deleted += errorHandler;
                errorWatcher.Error += new ErrorEventHandler(OnError);
                errorWatcher.EnableRaisingEvents = true;

                Watchers.Add(errorWatcher);

                foreach (var file in client.Files)
                {
                    var watchKey = string.Format("{0}{1}", file.DropFolderPathOverride ?? client.DropFolderPath, file.FilterPattern);
                    if (!uniqueWatcherHash.Contains(watchKey))
                    {
                        // these should watch the incomming folder (ftp drop endpoint)
                        var uploadWatcher = new FileSystemWatcher(file.DropFolderPathOverride ?? client.DropFolderPath, file.FilterPattern) {
                            NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName,
                            InternalBufferSize = 65536
                        };

                        uploadWatcher.Created += (source, e) => {
                            var info = CreateUploadInfo(client, file, e.FullPath);
                            DownloadQueue.Enqueue(info, fileReady: (i) => {
                                if (file.FileType == FileType.Delete)
                                    Delete(info.Path);
                                else if (!file.Upload)
                                    Archive(new UploadResult { // skip upload, move file to archive location
                                        UploadInfo = i,
                                        Success = true
                                    });
                                else
                                    UploadQueue.Enqueue(i, UploadResultHandler);
                            });
                        };

                        uploadWatcher.Renamed += (source, e) => {
                            var info = CreateUploadInfo(client, file, e.FullPath);
                            DownloadQueue.Enqueue(info, fileReady: (i) => {
                                if (file.FileType == FileType.Delete)
                                    Delete(info.Path);
                                else if (!file.Upload)
                                    Archive(new UploadResult { // skip upload, move file to archive location
                                        UploadInfo = i,
                                        Success = true
                                    });
                                else
                                    UploadQueue.Enqueue(i, UploadResultHandler);
                            });
                        };

                        uploadWatcher.Error += new ErrorEventHandler(OnError);
                        uploadWatcher.EnableRaisingEvents = true;

                        Watchers.Add(uploadWatcher);
                        uniqueWatcherHash.Add(watchKey);
                    }
                    else
                    {
                        Logger.WriteEntry(string.Format("WARNING ignoring duplicate file node for client: {0}, fileType: {1}, filterPattern: {2}", client.Name, file.FileType, file.FilterPattern), EventLogEntryType.Warning);
                    }

                    if (!file.Archive) continue;
                    // archive watchers watch the file location after dropoff
                    var archivePath = PathHelper.GetArchiveDir(Service.Instance.Config.OutputRootPath, client, file);

                    watchKey = string.Format("{0}{1}", archivePath, "*.*");
                    if (!uniqueWatcherHash.Contains(watchKey))
                    {
                        var archiveWatcher = new FileSystemWatcher(archivePath, "*.*") {
                            NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName,
                            InternalBufferSize = 65536
                        };

                        archiveWatcher.Created += (source, e) => {
                            // just a double check
                            if (!file.Archive) return;
                            var info = CreateUploadInfo(client, file, e.FullPath);
                            ArchiveQueue.Enqueue(e.FullPath, info, ArchiveResultHandler);
                        };

                        archiveWatcher.Error += new ErrorEventHandler(OnError);
                        archiveWatcher.EnableRaisingEvents = true;

                        Watchers.Add(archiveWatcher);
                        uniqueWatcherHash.Add(watchKey);
                    }
                }
            }
        }

        private void ArchiveResultHandler(ArchiveResult result)
        {
            if (result.Success)
            {
                Delete(result.Path);
            }
            else // retries are handled internally on the archive queue. move to exception.
            {
                Exception(result.Path, result.UploadInfo, result.Exception);
            }
        }

        public void UploadResultHandler(UploadResult result)
        {
            if (result.Success)
            {
                if(result.UploadInfo.Archive)
                    Archive(result);
            }
            else
            {
                if (result.Exception != null) { } // TODO: do we need a log of retries?
                if (result.UploadInfo.Retries < Util.Setting<int>("MaxRetries"))
                {
                    result.UploadInfo.Retries++;
                    UploadQueue.Enqueue(result.UploadInfo, UploadResultHandler);
                }
                else
                {
                    // move file to Exception folder
                    Exception(result.UploadInfo.Path, result.UploadInfo, result.Exception);
                }
            }
        }

        #endregion

        public void Reload(string key)
        {
            ReloadQueue.Enqueue(key);
        }

        private void Archive(UploadResult result)
        {
            var fileName = Path.GetFileName(result.UploadInfo.Path);
            
            if (UploadQueue.TestMode) return;

            var archivePath = Path.Combine(
                PathHelper.GetArchiveDir(Service.Instance.Config.OutputRootPath, result.UploadInfo),
                result.WasCompressed ? result.CompressionInfo.FileName : fileName
            );

            if (File.Exists(archivePath))
                File.Delete(archivePath);

            if (result.WasCompressed)
            {
                File.Move(result.CompressionInfo.TempPath, archivePath);
                // now delete the original file since the compressed version has been archived.
                File.Delete(result.UploadInfo.Path);
            }
            else
            {
                File.Move(result.UploadInfo.Path, archivePath);
            }
        }

        private void Delete(string path)
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch (Exception e)
            {
                Logger.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }
        }

        public void RetryException(ClientConfig client, FileConfig file, string fileName, RetryExceptionType type)
        {
            var exceptionPath = Path.Combine(Config.OutputRootPath,
                PathHelper.GetExceptionDir(Service.Instance.Config.OutputRootPath, client, file),
                fileName
            );

            var destinationPath = type == RetryExceptionType.Upload 
                ? Path.Combine(file.DropFolderPathOverride ?? client.DropFolderPath, fileName)
                : Path.Combine(PathHelper.GetArchiveDir(Service.Instance.Config.OutputRootPath, client, file), fileName);

            if (!File.Exists(exceptionPath) || File.Exists(destinationPath))
                return;

            if (UploadQueue.TestMode) return;

            try
            {
                // move back to client drop folder
                File.Move(exceptionPath, destinationPath);
                // delete error log file
                if (File.Exists(exceptionPath + ".error"))
                    File.Delete(exceptionPath + ".error");
            }
            catch (Exception e)
            {
                Logger.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }
        }

        public void Exception(string path, UploadInfo info, Exception exception)
        {
            var fileName = Path.GetFileName(path);

            // TODO send exception info to client?

            if (UploadQueue.TestMode) return;

            var exceptionPath = Path.Combine(PathHelper.GetExceptionDir(Service.Instance.Config.OutputRootPath, info), fileName);

            try
            {
                if (File.Exists(exceptionPath))
                    File.Delete(exceptionPath);

                File.Move(path, exceptionPath);

                if (exception != null)
                {
                    // add an exception log buddy file
                    var errorPath = exceptionPath + ".error";

                    if (File.Exists(errorPath))
                        File.Delete(errorPath);

                    using (var writer = File.CreateText(errorPath))
                        writer.Write(exception.ToString());
                    
                    var exstr = exception.ToString();
                    UploadHub.SendQueueItemEvent(new QueueItemEvent(info, QueueType.Unknown, EventType.Error) {
                        Exception = exstr
                    });
                    Logger.WriteEntry(exstr, EventLogEntryType.Error);
                }
            }
            catch (Exception e)
            {
                Logger.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }
        }

        public static UploadInfo CreateUploadInfo(ClientConfig client, FileConfig file, string path)
        {
            FileInfo fi = null;
            if (File.Exists(path))
                fi = new FileInfo(path);

            return new UploadInfo {
                Archive = file.Archive,
                FileSize = fi != null && fi.Exists ? fi.Length : default(long),
                Path = path,
                BlobContainer = file.BlobContainer,
                ClientName = client.Name,
                FileType = file.FileType,
                FileTypeOverride = file.FileTypeOverride,
                ArchiveRootOverride = file.ArchiveRootOverride,
                OutputSubFolder = file.OutputSubFolder,
                WorkerType = file.WorkerType,
                QueueName = file.QueueContainer
            };
        }

        //  This method is called when the FileSystemWatcher detects an error. 
        private static void OnError(object source, ErrorEventArgs e)
        {
            //  Show that an error has been detected.
            Console.WriteLine("The FileSystemWatcher has detected an error");
            //  Give more information if the error is due to an internal buffer overflow. 
            if (e.GetException().GetType() == typeof(InternalBufferOverflowException))
            {
                //  This can happen if Windows is reporting many file system events quickly  
                //  and internal buffer of the  FileSystemWatcher is not large enough to handle this 
                //  rate of events. The InternalBufferOverflowException error informs the application 
                //  that some of the file system events are being lost.
                Console.WriteLine(("The file system watcher experienced an internal buffer overflow: " + e.GetException().Message));
            }
            var logger = new EventLog(LogName, ".", LogSource);
            logger.WriteEntry(e.GetException().ToString(), EventLogEntryType.Error);
        }
    }

}
