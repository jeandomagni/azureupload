﻿using AzureUpload.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace AzureUpload.Data.Services
{
    public class SiteService : ServiceBase
    {
        public List<Site> GetSites(int chainId)
        {
            var sites = Connection.Query<Site>("select NPI, SiteNumber from Sites where ChainId = @chainId", new { chainId });
            return sites.ToList();
        }

        public List<Site> GetActiveSites(int chainId)
        {
            var sites = Connection.Query<Site>("select NPI, SiteNumber from Sites where ChainId = @chainId AND IsDeactivated = 0", new { chainId });
            return sites.ToList();
        }
    }
}