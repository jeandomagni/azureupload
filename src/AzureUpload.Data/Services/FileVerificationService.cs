﻿using AzureUpload.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using AzureUpload.Core.Models;

namespace AzureUpload.Data.Services
{
    public class FileVerificationService : ServiceBase
    {
        public List<FileVerificationSetting> GetChainFileSettings()
        {
            var settings = Connection.Query<FileVerificationSetting>("select ChainId, ClientName from FileVerificationSettings");
            return settings.ToList();
        }

        public void UpdateChainFileSettings(FileWatcherConfig config)
        {
            var settings = Connection.Query<FileVerificationSetting>("select ChainId, ClientName from FileVerificationSettings");
            var settingsMap = settings.ToDictionary(x => x.ChainId);

            string update = @"UPDATE FileVerificationSettings 
                                SET ClientName = @Name 
                                WHERE ChainId = @ChainId";
            string insert = @"insert into FileVerificationSettings(ChainId, ClientName) values(@ChainId, @Name)";

            foreach (var client in config.Clients)
            {
                if (client.ChainId < 1) continue;

                if (settingsMap.ContainsKey(client.ChainId)) // record exists in db
                {
                    if (client.Name != settingsMap[client.ChainId].ClientName)
                        Connection.Execute(update, new { client.ChainId, client.Name });
                }
                else // create
                {
                    Connection.Execute(insert, new { client.ChainId, client.Name });
                }
            }
        }
    }
}
