﻿using AzureUpload.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Data
{
    public abstract class ServiceBase : IDisposable
    {
        public SqlConnection Connection { get; private set; }
        
        public ServiceBase()
        {
            Connection = new SqlConnection(Util.Setting("SqlConnectionString"));
            Connection.Open();
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}
