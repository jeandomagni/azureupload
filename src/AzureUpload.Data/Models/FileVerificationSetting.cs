﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureUpload.Data.Models
{
    public class FileVerificationSetting
    {
        public int ChainId { get; set; }
        public string ClientName { get; set; }
    }
}
