﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.Data.Models
{
    public class Site
    {
        public int SiteNumber { get; set; }
        public string NPI { get; set; }
    }
}
