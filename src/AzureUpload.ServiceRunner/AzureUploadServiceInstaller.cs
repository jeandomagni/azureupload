﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.ServiceTester
{
    [RunInstaller(true)]
    public class AzureUploadServiceInstaller : Installer
    {
        private ServiceInstaller serviceInstaller;
        private ServiceProcessInstaller processInstaller;

        public AzureUploadServiceInstaller()
        {
            // Instantiate installers for process and services.
            processInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();

            serviceInstaller.DisplayName = "Azure Upload Service";
            serviceInstaller.Description = "Uploads files from the ftp dropoff to the azure blob stroage";
            serviceInstaller.ServiceName = "AzureUpload";
            serviceInstaller.StartType = ServiceStartMode.Manual;
            
            // The services run under the system account.
            processInstaller.Account = ServiceAccount.LocalSystem;

            // Add installers to collection. Order is not important.
            Installers.Add(serviceInstaller);
            Installers.Add(processInstaller);
        }
    }
}
