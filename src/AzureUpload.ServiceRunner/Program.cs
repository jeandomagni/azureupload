﻿using AzureUpload.Service;
using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AzureUpload.ServiceRunner
{
    public class Program
    {
        static void Main(string[] args)
        {
            if (!EventLog.SourceExists(Service.Service.LogSource))
                EventLog.CreateEventSource(Service.Service.LogSource, Service.Service.LogName);

            var logger = new EventLog(Service.Service.LogName, ".", Service.Service.LogSource);

            AppDomain.CurrentDomain.UnhandledException += (object sender, UnhandledExceptionEventArgs e) =>
            {
                var ex = (Exception)e.ExceptionObject;
                logger.WriteEntry(ex.ToString(), EventLogEntryType.Error);
            };
         
            if (args.Length != 0)
            {

                switch (args[0])
                {
                    case "/i":
                        logger.WriteEntry("Installing AzureUpload Service");
                        ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
                        Console.Read();
                        break;
                    case "/u":
                        logger.WriteEntry("Uninstalling AzureUpload Service");
                        ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
                        break;
                }
            }
            else
            {
                try
                {
#if DEBUG
                    RunDebug(logger);
#else
                    Run(logger);
#endif
                }
                catch (Exception ex) // should stop the program from crashing completely on an unhandled exception
                {
                    logger.WriteEntry(ex.ToString(), EventLogEntryType.Error);
                }
            }

        }

        static void Run(EventLog logger)
        {
            logger.WriteEntry("Running Service");
            var service = new Service.Service();
            service.Logger = logger;
            var services = new ServiceBase[] { service };
            ServiceBase.Run(services);
        }

        static void RunDebug(EventLog logger)
        {
            Console.WriteLine("------------DEBUGGING SERVICE-------------");
            Console.WriteLine("Service starting....");

            var service = new Service.Service();
            service.Logger = logger;
            service.DebuggerStart();

            Console.WriteLine("Service started.");
            Console.WriteLine("Press \"q\" to quit.");

            while (Console.ReadLine() != "q") ;

            service.Dispose();
        }


    }
}
