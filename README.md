# AzureUpload 
#### © [http://supplylogix.com](http://supplylogix.com)

----

## Overview
Azure Upload is a windows service project with a lightweight (OWIN) web server used to configure uploading local drive files to an Azure blob. There is also a web client used to connect and control the service.

Project Layout
    
    + build				(package, install, uninstall scripts)
    + src
        + service		(service source)
        + client		(web client source)
        + runner		(exe for running)
    + tools				(generate config markup from linqpad, etc)
    - AzureUpload.sln
    


## Deployment
There are two components to install:

### Prerequisite
Edit the `Runner\AzureUpload.ServiceRunner` project file. Under the `Signing` tab, click the `Create Test Certificate` button and enter a password. This will create a temporary certificate with which we can sign the executable.

### The Service
Under `./build` there are a few scripts for packaging, installing and uninstalling the service. To package the service run `pack.bat` thus generating a `.nupkg` with all the necessary resources for installing and running the service. 

Once you have generated the `.nupkg` file copy the package to the deployment server and extract the contents (rename to .zip and extract). You will see a tools folder with an `install.bat`. This will install the service. Once installed in the command line press any key to start the service.

####Example
On your local Machine

    > cd %SolutionDir%\build
    > pack.bat # generates AzureUpload.nupkg

Copy AzureUpload.nupkg to deployment server, rename to zip, extract

    > cd %CopiedPackageDir%\tools
    > install.bat # (press enter after installed to start the service)

Uninstall from server

    > cd %CopiedPackageDir%\tools
    > uninstall.bat
    
### The Web Client
Nothing special, copy `Views`, `bin`, `Scripts`, `Content`, and `web.config` to your webserver root.