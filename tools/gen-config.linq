<Query Kind="Program">
  <Reference Relative="..\src\AzureUpload.ServiceTester\bin\Release\AzureUpload.Service.dll">C:\dev\Git\supplylogix\AzureUpload\src\AzureUpload.ServiceTester\bin\Release\AzureUpload.Service.dll</Reference>
  <Reference Relative="..\src\AzureUpload.ServiceTester\bin\Release\AzureUpload.ServiceTester.exe">C:\dev\Git\supplylogix\AzureUpload\src\AzureUpload.ServiceTester\bin\Release\AzureUpload.ServiceTester.exe</Reference>
  <Reference Relative="..\packages\Newtonsoft.Json.4.5.11\lib\net40\Newtonsoft.Json.dll">C:\dev\Git\supplylogix\AzureUpload\packages\Newtonsoft.Json.4.5.11\lib\net40\Newtonsoft.Json.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Json.dll</Reference>
  <Reference Relative="..\packages\YamlDotNet.Core.1.2.1\lib\YamlDotNet.Core.dll">C:\dev\Git\supplylogix\AzureUpload\packages\YamlDotNet.Core.1.2.1\lib\YamlDotNet.Core.dll</Reference>
  <Reference Relative="..\packages\YamlDotNet.RepresentationModel.1.2.1\lib\YamlDotNet.RepresentationModel.dll">C:\dev\Git\supplylogix\AzureUpload\packages\YamlDotNet.RepresentationModel.1.2.1\lib\YamlDotNet.RepresentationModel.dll</Reference>
  <Namespace>AzureUpload.Service.Models</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>System.Xml.Serialization</Namespace>
  <Namespace>YamlDotNet.RepresentationModel.Serialization</Namespace>
</Query>

void Main()
{
	//Deserialize();
	//return;
	var config = new FileWatcherConfig {
      FTPRootPath = @"C:\dev\Fun\Uploader\FTP_IN",
	  OutputRootPath = @"C:\dev\fun\Uploader",
	  Status = Status.Running,
      Clients = new List<ClientConfig> {
          new ClientConfig { 
              DropFolderName = @"josiah_in", 
              Name = "Josiah",
			  Status = Status.Running,
              Files = new List<FileConfig>{
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.QOH, FilterPattern = "*qoh*.txt", QueueContainer ="worker05", WorkerType="QOH" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.Dispense, FilterPattern = "*dispense*.txt", QueueContainer ="worker05", WorkerType="Dispense" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.CreditReturn, FilterPattern = "*credit*.txt", QueueContainer ="worker05", WorkerType="CreditReturn" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.Adjustment, FilterPattern = "*adjustment*.txt", QueueContainer ="worker05", WorkerType="Adjustment" }
              }
          },
          new ClientConfig { 
              DropFolderName = @"josiah2_in", 
              Name = "Josiah2",
			  Status = Status.Running,
              Files = new List<FileConfig>{
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.QOH, FilterPattern = "*qoh*.txt", QueueContainer ="worker05", WorkerType="QOH" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.Dispense, FilterPattern = "*dispense*.txt", QueueContainer ="worker05", WorkerType="Dispense" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.CreditReturn, FilterPattern = "*credit*.txt", QueueContainer ="worker05", WorkerType="CreditReturn" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.Adjustment, FilterPattern = "*adjustment*.txt", QueueContainer ="worker05", WorkerType="Adjustment" }
              }
          },
		  new ClientConfig { 
              DropFolderName = @"josiah3_in", 
              Name = "Josiah 3",
			  Status = Status.Running,
              Files = new List<FileConfig>{
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.QOH, FilterPattern = "*qoh*.txt", QueueContainer ="worker05", WorkerType="QOH" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.Dispense, FilterPattern = "*dispense*.txt", QueueContainer ="worker05", WorkerType="Dispense" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.CreditReturn, FilterPattern = "*credit*.txt", QueueContainer ="worker05", WorkerType="CreditReturn" },
                  new FileConfig{ BlobContainer = "workerincoming", FileType = FileType.Adjustment, FilterPattern = "*adjustment*.txt", QueueContainer ="worker05", WorkerType="Adjustment" }
              }
          }
      }
  };
  
  var usejson = true;
  var myDir = Path.GetDirectoryName (Util.CurrentQueryPath);
  var outputPath = Path.Combine(myDir, string.Format("..\\config.{0}", usejson ? "json" : "xml"));
  string.Format("Config File output to {0}", outputPath).Dump();

  using(var writer = new StreamWriter(outputPath))
  {
	if(!usejson)
	{
		var xns = new XmlSerializerNamespaces();
		xns.Add(string.Empty, string.Empty);
		var ser =  new XmlSerializer(typeof(FileWatcherConfig));
		ser.Serialize(writer, config, xns);
	}
	else
	{
		var json = JsonConvert.SerializeObject(config, Formatting.Indented);
		writer.Write(json);
	}
  }
  
}

public void Deserialize()
{
	var yaml = true;
	var myDir = Path.GetDirectoryName (Util.CurrentQueryPath);
	var outputPath = Path.Combine(myDir, string.Format("..\\config.{0}", yaml ? "txt" : "xml"));
	string.Format("Config File output to {0}", outputPath).Dump();
	var serializer = new YamlSerializer();
	
	using (TextReader reader = File.OpenText(outputPath))
	{
		var obj = serializer.Deserialize(reader);
		obj.Dump();
	}
	
}